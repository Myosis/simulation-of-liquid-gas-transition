!----------SUBROUTINES----------!

MODULE SUBROUTINES
    logical :: relativistic
	contains
SUBROUTINE MERCURY_POTENTIAL(r,Pot)
	 integer :: i
	 real(8), intent(in) :: r
	 real(8), intent(out) :: Pot
	 real(8), dimension(0:8) :: coef
	 Pot = 0
     if(relativistic)then
	 coef(0)=-392.d0
	 coef(1)=0.d0
	 coef(2)=534077.3126007207d0
	 coef(3)=-2.048297635930095d7
	 coef(4)=2.9552605727198446d8
	 coef(5)=-2.1155539832612858d9
	 coef(6)=8.125997490712963d9
	 coef(7)=-1.607981490556498d10
	 coef(8)=1.2894913865326828d10
     else
     coef(0)=-1100.d0
	 coef(1)=0.d0
	 coef(2)=2.8492974083327814d6
	 coef(3)=-1.1299413271171196d8
	 coef(4)=1.8382967116783445d9
	 coef(5)=-1.5576165381432028d10
	 coef(6)=7.287938220493896d10
	 coef(7)=-1.792577619810004d11
	 coef(8)=1.8155145199668713d11
     end if
	 do i=0,8
			Pot=Pot+coef(i)/r**(i+6)
	 end do
end SUBROUTINE MERCURY_POTENTIAL

SUBROUTINE READ_INPUTFILE(NbPartTot,NbPart,BoxL,PosParts,Temp,Nb1Loop,Nb2Loop)
	integer, dimension(1:2), intent(out) :: NbPart
	integer, intent(out) :: NbPartTot
	integer, intent(in) :: Nb1Loop,Nb2Loop
	integer :: i,j
	real(8), dimension(1:2), intent(out) :: BoxL
	real(8), dimension(1:2) :: Density
	real(8) :: Ratio
	real(8), intent(out) :: Temp
	real(8), dimension(:,:,:), allocatable, intent(out) :: PosParts
	CHARACTER(len=32) :: InputFilePath,TempString,DensityString
	CHARACTER(len=4) :: relatString

  CALL GET_COMMAND_ARGUMENT(1, InputFilePath)
	CALL GET_COMMAND_ARGUMENT(2, TempString)
	CALL GET_COMMAND_ARGUMENT(3, DensityString)
	CALL GET_COMMAND_ARGUMENT(4, relatString)


	IF (LEN_TRIM(InputFilePath) == 0) then
		STOP 'No input file as argument'
	else
		open(12, file=TRIM(InputFilePath),access='sequential', form='formatted', status="old", action="read")
		read(12,*) NbPartTot,NbPart(1),NbPart(2)
		read(12,*) BoxL(1),BoxL(2)
		allocate(PosParts(1:2,0:NbPartTot-1,0:2))
		do i=0,NbPart(1)-1
			read(12,113) WhichParts,PosParts(1,i,:)
			113 format (A3,3ES15.7) !implement argument
		end do
		do i=0,NbPart(2)-1
			read(12,114) WhichParts,PosParts(2,i,:)
			114 format (A3,3ES15.7) !implement argument
		end do
		close(12)
	end if

	IF (LEN_TRIM(TempString) == 0) then
		STOP 'No Temperature as argument'
	else
		TempString = TRIM(TempString)
		read(TempString,*) Temp
	end if

  IF (LEN_TRIM(DensityString) == 0) then
        STOP 'No Density as argument'
  else
    DensityString = TRIM(DensityString)
    read(DensityString,*) DensityTot
    Ratio = ((NbPartTot/(BoxL(1)**3+Boxl(2)**3))/DensityTot)**(1/3.0)
    BoxL = BoxL*Ratio
    PosParts(1,:,:) = Ratio*PosParts(1,:,:)
    PosParts(2,:,:) = Ratio*PosParts(2,:,:)
  end if

  IF (LEN_TRIM(relatString) == 0) then
		STOP 'No Relativistic as argument'
	else
		IF (TRIM(relatString) == '0') then
            relativistic=.False.
        else
            relativistic=.True.
        end if
	end if

	open(12, file="simulation.txt",access='sequential', form='formatted', status="unknown", action="write")
	write(12,*) 'Number of particules',NbPart(1),NbPart(2)
	write(12,*) 'Volumes',BoxL(1)**3,BoxL(2)**3
	write(12,*) 'Temperature',Temp
	write(12,*) 'Number of loops',Nb1Loop,Nb2Loop
	write(12,*) 'Relativistic',relativistic
	close(12)
END SUBROUTINE READ_INPUTFILE

SUBROUTINE WRITE_OUTPUTFILE(NbPartTot,NbPart,BoxL,Nb1Loop,Nb12Loop,NbPair,PosParts,MeanPotential&
		&,MeanVolume,MeanEnthalpy,ListBoxL,ListNbPart,VolumeTot,ListPotential,ListMu,MeanNbPart,MeanMu)

	integer, intent(in) :: NbPartTot,Nb12Loop,NbPair
	integer, dimension(1:2), intent(in) :: NbPart
	integer :: i
	integer, dimension(1:2,0:Nb12Loop-1) ::ListNbPart
	real(8), dimension(1:2), intent(in) :: MeanPotential,BoxL,MeanVolume,MeanEnthalpy,MeanNbPart,MeanMu
	real(8), dimension(1:2,0:NbPartTot-1,0:2),intent(in) :: PosParts
	real(8), dimension(1:2,0:Nb12Loop-1) :: ListBoxL, ListPotential
	real(8), dimension(1:2,Nb1Loop-1) :: ListMu
	real(8), intent(in) :: VolumeTot
	CHARACTER(len=3) :: WhichParts = 'Ar '

	open(12, file="VolumeEvol.dat", status="unknown", access="stream", action="write")
	write(12) ListBoxL**3/VolumeTot
	close(12)

	open(12, file="NbPartEvol.dat", status="unknown", access="stream", action="write")
	write(12) ListNbPart/Real(NbPartTot)
	close(12)

	open(12, file="PotentialEvol.dat", status="unknown", access="stream", action="write")
	write(12) ListPotential
	close(12)

	open(12, file="MuEvol.dat", status="unknown", access="stream", action="write")
	write(12) ListMu
	close(12)

	open(12, file="simulation.txt",access='append', form='formatted', status="old", action="write")
	write(12,*) 'Potential         ',MeanPotential
	write(12,*) 'Enthalpy          ',MeanEnthalpy
	write(12,*) 'Volume            ',MeanVolume
	write(12,*) 'NbPart            ',MeanNbPart
	write(12,*) 'Density           ',MeanNbPart/MeanVolume
	write(12,*) 'DensityEnd        ',NbPart/BoxL**3
	close(12)

	open(12, file="gibbs.of",access='sequential', form='formatted', status="unknown", action="write")
	write(12,*) SUM(NbPart),NbPart(1),NbPart(2)
	write(12,*) BoxL(1),BoxL(2)
	do i=0,NbPart(1)-1
		write(12,113) WhichParts,PosParts(1,i,:)
		113 format (A3,3ES15.7)
	end do
	do i=0,NbPart(2)-1
		write(12,114) WhichParts,PosParts(2,i,:)
		114 format (A3,3ES15.7)
	end do
	close(12)
END SUBROUTINE WRITE_OUTPUTFILE

SUBROUTINE POTENTIAL_INIT(NbPartTot,NbPart,PosParts,PairsPot,BoxL,Potential,Rcut)

	real(8), intent(in) :: BoxL, Rcut
	real(8), intent(out) :: Potential
	integer, intent(in) :: NbPart,NbPartTot
	integer :: i=0,j=0
	real(8), dimension(0:NbPartTot-1,0:2),intent(in) :: PosParts
	real(8), dimension(0:2) :: PosPeriodicParts_ij
	real(8), dimension(0:NbPartTot-1,0:NbPartTot-1),intent(out) :: PairsPot
	real(8) :: r_ij
	PairsPot = 0
	Potential = 0

	do i=0,NbPart-2
		do j=i+1,NbPart-1
			PosPeriodicParts_ij = PosParts(i,:)-PosParts(j,:)-BoxL*ANINT((PosParts(i,:)-PosParts(j,:))/BoxL)
			r_ij = SQRT(SUM((PosPeriodicParts_ij)**2))
			if (r_ij.LT.Rcut) then !Potential truncation?
				call MERCURY_POTENTIAL(r_ij,PairsPot(i,j))
				PairsPot(j,i) = PairsPot(i,j)
				Potential = Potential + PairsPot(i,j)
			else
				PairsPot(i,j) = 0
				PairsPot(j,i) = PairsPot(i,j)
			end if
		end do
	end do
END SUBROUTINE POTENTIAL_INIT

SUBROUTINE RANDOM_INIT()

	integer :: k
	integer, dimension(0:7) :: values
	integer, dimension(:), allocatable :: seed
	call date_and_time(values=values)
	call RANDOM_SEED(size=k)
	allocate(seed(1:k))
	seed(:) = values(7)
	call RANDOM_SEED(put=seed)
END SUBROUTINE RANDOM_INIT

SUBROUTINE TRIAL_PARTICULE_DISPLACEMENT(NbPartTot,NbPart,PosParts,PairsPot,BoxL,Potential,Rcut,&
		&AmpPD,beta,NbAcceptedPD,NbCallPD)
	integer :: TempPart, i=0,b
	integer, intent(in) :: NbPartTot
	integer, dimension(1:2), intent(in) :: NbPart
	integer, dimension(1:2), intent(inout) :: NbAcceptedPD,NbCallPD
	real(8) :: Random,acceptation,Bolzmann
	real(8), dimension(0:2) :: Displacement, PosPeriodicParts_ij
	real(8), dimension(1:2,0:NbPartTot-1,0:NbPartTot-1),intent(inout) :: PairsPot
	real(8), dimension(0:NbPartTot-1,0:NbPartTot-1) :: TempPairsPot
	real(8), intent(in) :: beta
	real(8), dimension(1:2), intent(in) :: AmpPD,BoxL,Rcut
	real(8), dimension(1:2), intent(inout) :: Potential
	real(8), dimension(1:2,0:NbPartTot-1,0:2), intent(inout) :: PosParts
	real(8) :: r_ij

	call RANDOM_NUMBER(Random)
	TempPart = FLOOR(Random*NbPartTot)
	call RANDOM_NUMBER(Displacement)

	if (TempPart.LT.NbPart(1)) then !!revoir la distri des proba entre 1 et 2
		b = 1
	else
		b = 2
		TempPart = TempPart - NbPart(1)
    end if

    NbCallPD(b) = NbCallPD(b) + 1

	TempPairsPot = PairsPot(b,:,:)
	Displacement = AmpPD(b)*(Displacement-0.5)*BoxL(b)

	do i=0,NbPart(b)-1
		if (i.NE.TempPart) then
			PosPeriodicParts_ij = PosParts(b,i,:)-(PosParts(b,TempPart,:)+displacement) &
				&-BoxL(b)*ANINT((PosParts(b,i,:)-(PosParts(b,TempPart,:)+displacement))/BoxL(b))
			r_ij = SQRT(SUM((PosPeriodicParts_ij)**2))
			if (r_ij.LT.Rcut(b)) then !Potential truncation?
				call MERCURY_POTENTIAL(r_ij,TempPairsPot(i,TempPart))
				TempPairsPot(TempPart,i) = TempPairsPot(i,TempPart)
			else
				TempPairsPot(i,TempPart) = 0
				TempPairsPot(TempPart,i) = TempPairsPot(i,TempPart)
			end if
		end if
	end do
	TempPartPot = SUM(TempPairsPot(TempPart,:))
	PartPot = SUM(PairsPot(b,TempPart,:))

	call RANDOM_NUMBER(acceptation)
	Boltzmann = EXP(-beta*(TempPartPot-PartPot))
	if (acceptation.LT.Boltzmann) then
		Potential(b) = Potential(b) + TempPartPot - PartPot
		NbAcceptedPD(b)=NbAcceptedPD(b)+1
		PairsPot(b,:,:) = TempPairsPot
		PosParts(b,TempPart,:) = PosParts(b,TempPart,:)+Displacement-BoxL(b)*ANINT((PosParts(b,TempPart,:)+Displacement)/BoxL(b))
	end if
END SUBROUTINE TRIAL_PARTICULE_DISPLACEMENT

SUBROUTINE TRIAL_VOLUME_CHANGE(NbPartTot,VolumeTot,NbPart,PosParts,PairsPot,BoxL,Potential,Rcut,&
		&AmpVC,beta,NbAcceptedVC)
	integer :: i=0
	integer, intent(in) :: NbPartTot
	integer, dimension(1:2), intent(in) :: NbPart
	integer, intent(inout) :: NbAcceptedVC
	real(8) :: acceptation, Boltzmann
	real(8), dimension(1:2) :: TempPotential, TempBoxL, RatioD, VolChange, TempRcut, Vol
	real(8), dimension(1:2,0:NbPartTot-1,0:NbPartTot-1),intent(out) :: PairsPot
	real(8), dimension(1:2,0:NbPartTot-1,0:NbPartTot-1) :: TempPairsPot
	real(8), intent(in) :: AmpVC,beta,VolumeTot
	real(8), dimension(1:2), intent(inout) :: Potential,BoxL,Rcut
	real(8), dimension(1:2, 0:NbPartTot-1,0:2), intent(inout) :: PosParts
	real(8), dimension(1:2, 0:NbPartTot-1,0:2) :: TempPosParts

	call RANDOM_NUMBER(VolChange(1))
	Vol=BoxL**3
	VolChange(1) = LOG(Vol(1)/Vol(2))+AmpVC*(VolChange(1)-0.5)
	VolChange(1) = VolumeTot*EXP(VolChange(1))/(1+EXP(VolChange(1)))
	VolChange(2) = VolumeTot-VolChange(1)
	TempBoxL = VolChange**(1/3.0)
	RatioD = TempBoxL/BoxL
	TempPosParts(1,:,:) = RatioD(1)*PosParts(1,:,:)
	TempPosParts(2,:,:) = RatioD(2)*PosParts(2,:,:)
	TempPairsPot = 0
	TempPotential = 0
	TempRcut = TempBoxL/2.0

	call POTENTIAL_INIT(NbPartTot,NbPart(1),TempPosParts(1,:,:),TempPairsPot(1,:,:),&
        &TempBoxL(1),TempPotential(1),TempRcut(1))
	call POTENTIAL_INIT(NbPartTot,NbPart(2),TempPosParts(2,:,:),TempPairsPot(2,:,:),&
        &TempBoxL(2),TempPotential(2),TempRcut(2))

	call RANDOM_NUMBER(acceptation)
	Boltzmann = (VolChange(1)/Vol(1))**(NbPart(1)+1)*(VolChange(2)/Vol(2))**(NbPart(2)+1)&
		&*EXP(-beta*(SUM(TempPotential)-SUM(Potential)))

	if (acceptation.LT.Boltzmann) then
		Potential = TempPotential
		NbAcceptedVC=NbAcceptedVC+1
		PairsPot = TempPairsPot
		PosParts = TempPosParts
		BoxL = TempBoxL
		Rcut = TempRcut
	end if
END SUBROUTINE TRIAL_VOLUME_CHANGE

SUBROUTINE TRIAL_PARTICULE_EXCHANGE(NbPartTot,NbPart,PosParts,PairsPot,BoxL,Potential,Rcut,&
		&Mu,beta,NbCallPE,NbAcceptedPE)
	integer :: TempPart, in, out
	integer, intent(in) :: NbPartTot
	integer, intent(inout) :: NbAcceptedPE
	integer, dimension(1:2), intent(inout) :: NbPart,NbCallPE
	real(8) :: Random, acceptation, Boltzmann, r_ij
	real(8), dimension(1:2) :: TempPotential, TempBoxL
	real(8), dimension(0:2) :: Displacement,PosPeriodicParts_ij
	real(8), dimension(1:2,0:NbPartTot-1,0:NbPartTot-1),intent(inout) :: PairsPot
	real(8), dimension(0:NbPartTot) :: PartInPot
	real(8), intent(in) :: beta
	real(8), dimension(1:2), intent(inout) :: Potential, Mu
	real(8), dimension(1:2), intent(in) :: BoxL,Rcut
	real(8), dimension(1:2, 0:NbPartTot-1,0:2), intent(inout) :: PosParts

	call RANDOM_NUMBER(Random)
	in = INT(Random*2)+1
	out = -1*INT(Random*2)+2
	call RANDOM_NUMBER(Random)
	TempPart = INT(Random*NbPart(out))
	call RANDOM_NUMBER(Displacement)
	Displacement = (Displacement-0.5)*BoxL(in)
	PartInPot = 0
	NbCallPE(in) = NbCallPE(in)+1

	do i=0,NbPart(in)-1
		PosPeriodicParts_ij = PosParts(in,i,:)-Displacement&
			&-BoxL(in)*ANINT((PosParts(in,i,:)-Displacement)/BoxL(in))
		r_ij = SQRT(SUM((PosPeriodicParts_ij)**2))
		if (r_ij.LT.Rcut(in)) then !Potential truncation?
			call MERCURY_POTENTIAL(r_ij,PartInPot(i))
		else
			PartInPot(i) = 0
		end if
	end do

	Mu(in) = Mu(in) + (BoxL(in)**3/(NbPart(in)+1))*EXP(-beta*SUM(PartInPot))!!!!!!!!

	TempPotential(in) = Potential(in) + SUM(PartInPot)
	TempPotential(out) = Potential(out) - SUM(PairsPot(out,TempPart,:))

	call RANDOM_NUMBER(acceptation)
	Boltzmann = ((NbPart(out)*BoxL(in)**3)/((NbPart(in)+1)*BoxL(out)**3))&
		&*EXP(-beta*(SUM(TempPotential)-SUM(Potential)))
	if (acceptation.LT.Boltzmann) then
		Potential = TempPotential

		PosParts(in,NbPart(in),:) = Displacement
		PosParts(out,TempPart,:) = PosParts(out,NbPart(out)-1,:)
		PosParts(out,Nbpart(out)-1,:) = 0

		PairsPot(out,TempPart,:) = PairsPot(out,NbPart(out)-1,:)
		PairsPot(out,NbPart(out)-1,:) = 0
		PairsPot(out,:,NbPart(out)-1) = 0
		PairsPot(out,TempPart,TempPart) = 0
		PairsPot(out,:,TempPart) = PairsPot(out,TempPart,:)
        PairsPot(in,NbPart(in),0:NbPart(in)-1) = PartInPot(0:NbPart(in)-1)
		PairsPot(in,0:NbPart(in)-1,NbPart(in)) = PartInPot(0:NbPart(in)-1)

		NbPart(in) = NbPart(in)+1
		NbPart(out) = NbPart(out)-1
		NbAcceptedPE = NbAcceptedPE+1
	end if
END SUBROUTINE TRIAL_PARTICULE_EXCHANGE

SUBROUTINE AMP_UPDATES(Amp,NbAccepted,NbCall)
	integer, intent(inout) :: NbAccepted, NbCall
	real(8), intent(inout) :: Amp
	if ((NbAccepted/REAL(NbCall)).LT.0.4) then !working for VC?
		Amp = Amp*0.9
	else if	((NbAccepted/REAL(NbCall)).GT.0.6) then
		Amp = Amp*1.1
	end if
	NbAccepted = 0
	NbCall = 0
END SUBROUTINE AMP_UPDATES

END MODULE SUBROUTINES

!----------MAIN----------!

PROGRAM LIQUID_POT

	USE SUBROUTINES
	IMPLICIT NONE
	integer :: NbPartTot,Nb1Loop,Nb2Loop,NbPair,Nb12Loop,Nb3Loop
	integer :: i,j,k,l=0,NbAcceptedVC=0,NbCallVC=0,NbAcceptedPE=0
	integer, dimension(1:2) :: NbCallPE=0,NbPart,NbAcceptedPD=0,NbCallPD=0
	integer, dimension(:,:), allocatable :: ListNbPart
	real(8), parameter :: Kb=3.1668114e-6
	real(8) :: AmpVC=0.2,beta,Temp,Random,VolumeTot
	real(8), dimension(:,:,:), allocatable :: PairsPot, PosParts
	real(8), dimension(:,:), allocatable :: ListPotential, ListBoxL, ListMu
	real(8), dimension(1:2) :: Potential, BoxL, MeanBoxL, MeanVolume, MeanEnthalpy, MeanNbPart, MeanMu, Rcut,&
		&AmpPD=0.1, MeanPotential, Cv, Mu, MeanDens

	Nb1Loop=200
	Nb2Loop=100

	call READ_INPUTFILE(NbPartTot,NbPart,BoxL,PosParts,Temp,Nb1Loop,Nb2Loop)


	Rcut = BoxL/2
	VolumeTot = BoxL(2)**3+BoxL(1)**3

	Nb3Loop=2*NbPartTot
	NbPair=((NbPartTot-1)*NbPartTot)/2
	Nb12Loop=Nb1Loop*Nb2Loop

	allocate(PairsPot(1:2,0:NbPartTot-1,0:NbPartTot-1))
	allocate(ListPotential(1:2,0:Nb12Loop-1))
	allocate(ListBoxL(1:2,0:Nb12Loop-1))
	allocate(ListNbPart(1:2,0:Nb12Loop-1))
	allocate(ListMu(1:2,0:Nb1Loop-1))


	beta = 1/(Kb*Temp)
	PairsPot=0

       	call POTENTIAL_INIT(NbPartTot,NbPart(1),PosParts(1,:,:),PairsPot(1,:,:),BoxL(1),Potential(1),Rcut(1))
	call POTENTIAL_INIT(NbPartTot,NbPart(2),PosParts(2,:,:),PairsPot(2,:,:),BoxL(2),Potential(2),Rcut(2))
	call RANDOM_INIT()
	do i=0,Nb1Loop-1 !1Loop Updates of the displacement amplitude parameter
          Mu = 0
		do j=0,Nb2Loop-1 !2Loop Updates of the physical quantities
			do k=0,Nb3Loop-1 !3Loop Uncorrelation of physical observables
				call RANDOM_NUMBER(Random)
				if (Random .LT. 0.5) then !ajouter le potentiel chimique
						call TRIAL_PARTICULE_EXCHANGE(NbPartTot,NbPart,PosParts,PairsPot,BoxL,Potential,Rcut,&
							&Mu,beta,NbCallPE,NbAcceptedPE)
				else if (Random .LT. 0.51) then
						call TRIAL_VOLUME_CHANGE(NbPartTot,VolumeTot,NbPart,PosParts,PairsPot,BoxL,Potential,Rcut,&
								&AmpVC,beta,NbAcceptedVC)
						NbCallVC = NbCallVC+1
				else
						call TRIAL_PARTICULE_DISPLACEMENT(NbPartTot,NbPart,PosParts,PairsPot,BoxL,Potential,Rcut,&
							&AmpPD,beta,NbAcceptedPD,NbCallPD)
				end if
			end do
			ListPotential(:,i*Nb2Loop+j) = Potential
			ListBoxL(:,i*Nb2Loop+j) = BoxL
			ListNbPart(:,i*Nb2Loop+j) = NbPart
		end do
		ListMu(:,i) = -(1/beta)*LOG(Mu/NbCallPE) !add thermal Brooglie wavelength
		print*,'PD',NbCallPD,NbAcceptedPD,AmpPD
		print*,'VC',NbCallVC,NbAcceptedVC,AmpVC
		print*,'PE',NbCallPE,NbAcceptedPE
		call AMP_UPDATES(AmpPD(1),NbAcceptedPD(1),NbCallPD(1))
		call AMP_UPDATES(AmpPD(2),NbAcceptedPD(2),NbCallPD(2))
		call AMP_UPDATES(AmpVC,NbAcceptedVC,NbCallVC)
		NbCallPE = 0
		NbAcceptedPE = 0
		print*,'Loading...',(i*100)/Nb1Loop,'%'
	end do


	MeanPotential = SUM(ListPotential(:,Nb12Loop/2-1:),DIM=2)/(Nb12Loop/2)
	MeanBoxL = SUM(ListBoxL(:,Nb12Loop/2-1:),DIM=2)/(Nb12Loop/2)
	MeanVolume = SUM(ListBoxL(:,Nb12Loop/2-1:)**3,DIM=2)/(Nb12Loop/2)
	MeanNbPart = SUM(ListNbPart(:,Nb12Loop/2-1:),DIM=2)/Real(Nb12Loop/2)
	MeanMu = SUM(ListMu(:,Nb1Loop/2-1:),DIM=2)/(Nb1Loop/2)
	MeanDens = SUM(ListNbPart(:,Nb12Loop/2-1:)/ListBoxL(:,Nb12Loop/2-1:)**3,DIM=2)/(Nb12Loop/2)
	Rcut = MeanBoxL/2

	call WRITE_OUTPUTFILE(NbPartTot,NbPart,BoxL,Nb1Loop,Nb12Loop,NbPair,PosParts,MeanPotential&
		&,MeanVolume,MeanEnthalpy,ListBoxL,ListNbPart,VolumeTot,ListPotential,ListMu,MeanNbPart,MeanMu)

	deallocate(PosParts)
	deallocate(PairsPot)
	deallocate(ListPotential)
	deallocate(ListNbPart)
	deallocate(ListMu)
	deallocate(ListBoxL)
END PROGRAM LIQUID_POT
