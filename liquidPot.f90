!----------SUBROUTINES----------!

MODULE SUBROUTINES
	contains
SUBROUTINE LJ_POTENTIAL(r,Pot)
	 real(8), intent(in) :: r
	 real(8), intent(out) :: Pot
   Pot = 4*((1.0/r)**12-(1.0/r)**6)
end SUBROUTINE LJ_POTENTIAL

SUBROUTINE READ_INPUTFILE(NbPart,BoxL,PosParts,Temp,Nb1Loop,Nb2Loop)
	integer, intent(out) :: NbPart
	integer, intent(in) :: Nb1Loop,Nb2Loop
	integer :: i,j
  real(8) :: Density, Ratio
	real(8), intent(out) :: BoxL
	real(8), intent(out) :: Temp
	real(8), dimension(:,:), allocatable, intent(out) :: PosParts
	CHARACTER(len=32) :: InputFilePath,TempString,DensityString

  CALL GET_COMMAND_ARGUMENT(1, InputFilePath)
	CALL GET_COMMAND_ARGUMENT(2, TempString)
	CALL GET_COMMAND_ARGUMENT(3, DensityString)


	IF (LEN_TRIM(InputFilePath) == 0) then
		STOP 'No input file as argument'
	else
		open(12, file=TRIM(InputFilePath),access='sequential', form='formatted', status="old", action="read")
		read(12,*) NbPart
		read(12,*) BoxL
		allocate(PosParts(0:NbPartTot-1,0:2))
		do i=0,NbPart-1
			read(12,113) WhichParts,PosParts(i,:)
			113 format (A3,3ES15.7) !implement argument
		end do
		close(12)
	end if

	IF (LEN_TRIM(TempString) == 0) then
		STOP 'No Temperature as argument'
	else
		TempString = TRIM(TempString)
		read(TempString,*) Temp
	end if

  IF (LEN_TRIM(DensityString) == 0) then
        STOP 'No Density as argument'
  else
    DensityString = TRIM(DensityString)
    read(DensityString,*) Density
    Ratio = ((NbPart/BoxL**3)/Density)**(1/3.0)
    BoxL = BoxL*Ratio
    PosParts(:,:) = Ratio*PosParts(:,:)
    PosParts(:,:) = Ratio*PosParts(:,:)
  end if

	open(12, file="simulation.txt",access='sequential', form='formatted', status="unknown", action="write")
	write(12,*) 'Number of particules',NbPart
	write(12,*) 'Volumes',BoxL**3
	write(12,*) 'Temperature',Temp
	write(12,*) 'Number of loops',Nb1Loop,Nb2Loop
	close(12)
END SUBROUTINE READ_INPUTFILE

SUBROUTINE WRITE_OUTPUTFILE(NbPart,BoxL,Nb1Loop,Nb12Loop,NbPair,PosParts,RadialDistribution,MeanPotential&
		&,Pressure,ListPotential)

	integer, intent(in) :: Nb12Loop,NbPair
	integer, intent(in) :: NbPart
	integer :: i
	integer, dimension(0:Nb12Loop-1) ::ListNbPart
	real(8), intent(in) :: MeanPotential,BoxL,Pressure
	real(8), dimension(0:NbPart-1,0:2),intent(in) :: PosParts
	real(8), dimension(0:Nb12Loop*NbPair-1), intent(in) :: RadialDistribution
	real(8), dimension(0:Nb12Loop-1) :: ListPotential
	CHARACTER(len=3) :: WhichParts = 'Ar '

	open(12, file="VolumeEvol.dat", status="unknown", access="stream", action="write")
	write(12) ListBoxL**3
	close(12)

	open(12, file="PotentialEvol.dat", status="unknown", access="stream", action="write")
	write(12) ListPotential
	close(12)

	open(12, file="simulation.txt",access='append', form='formatted', status="old", action="write")
	write(12,*) 'Potential         ',MeanPotential
	write(12,*) 'Pressure          ',Pressure
	close(12)

	open(12, file="gibbs.of",access='sequential', form='formatted', status="unknown", action="write")
	write(12,*) NbPart
	write(12,*) BoxL
	do i=0,NbPart-1
		write(12,113) WhichParts,PosParts(i,:)
		113 format (A3,3ES15.7)
	end do
	close(12)
END SUBROUTINE WRITE_OUTPUTFILE

SUBROUTINE POTENTIAL_INIT(NbPart,PosParts,PairsPot,BoxL,Potential,Rcut)

	real(8), intent(in) :: BoxL, Rcut
	real(8), intent(out) :: Potential
	integer, intent(in) :: NbPart
	integer :: i=0,j=0
	real(8), dimension(0:NbPart-1,0:2),intent(in) :: PosParts
	real(8), dimension(0:2) :: PosPeriodicParts_ij
	real(8), dimension(0:NbPart-1,0:NbPart-1),intent(out) :: PairsPot
	real(8) :: r_ij
	PairsPot = 0
	Potential = 0

	do i=0,NbPart-2
		do j=i+1,NbPart-1
			PosPeriodicParts_ij = PosParts(i,:)-PosParts(j,:)-BoxL*ANINT((PosParts(i,:)-PosParts(j,:))/BoxL)
			r_ij = SQRT(SUM((PosPeriodicParts_ij)**2))
			if (r_ij.LT.Rcut) then !Potential truncation?
				call LJ_POTENTIAL(r_ij,PairsPot(i,j))
				PairsPot(j,i) = PairsPot(i,j)
				Potential = Potential + PairsPot(i,j)
			else
				PairsPot(i,j) = 0
				PairsPot(j,i) = PairsPot(i,j)
			end if
		end do
	end do
END SUBROUTINE POTENTIAL_INIT

SUBROUTINE RANDOM_INIT()

	integer :: k
	integer, dimension(0:7) :: values
	integer, dimension(:), allocatable :: seed
	call date_and_time(values=values)
	call RANDOM_SEED(size=k)
	allocate(seed(1:k))
	seed(:) = values(7)
	call RANDOM_SEED(put=seed)
END SUBROUTINE RANDOM_INIT

SUBROUTINE TRIAL_PARTICULE_DISPLACEMENT(NbPart,PosParts,PairsPot,BoxL,Potential,Rcut,&
		&AmpPD,beta,NbAcceptedPD,NbCallPD)
	integer :: TempPart, i=0,b
	integer, intent(in) :: NbPart
	integer, intent(inout) :: NbAcceptedPD,NbCallPD
	real(8) :: Random,acceptation,Bolzmann
	real(8), dimension(0:2) :: Displacement, PosPeriodicParts_ij
	real(8), dimension(0:NbPart-1,0:NbPart-1),intent(inout) :: PairsPot
	real(8), dimension(0:NbPart-1,0:NbPart-1) :: TempPairsPot
	real(8), intent(in) :: beta
	real(8), intent(in) :: AmpPD,BoxL,Rcut
	real(8), intent(inout) :: Potential
	real(8), dimension(0:NbPart-1,0:2), intent(inout) :: PosParts
	real(8) :: r_ij

	call RANDOM_NUMBER(Random)
	TempPart = FLOOR(Random*NbPartTot)
	call RANDOM_NUMBER(Displacement)

  NbCallPD = NbCallPD + 1

	TempPairsPot = PairsPot(:,:)
	Displacement = AmpPD*(Displacement-0.5)*BoxL

	do i=0,NbPart-1
		if (i.NE.TempPart) then
			PosPeriodicParts_ij = PosParts(i,:)-(PosParts(TempPart,:)+displacement) &
				&-BoxL(b)*ANINT((PosParts(i,:)-(PosParts(TempPart,:)+displacement))/BoxL)
			r_ij = SQRT(SUM((PosPeriodicParts_ij)**2))
			if (r_ij.LT.Rcut) then !Potential truncation?
				call LJ_POTENTIAL(r_ij,TempPairsPot(i,TempPart))
				TempPairsPot(TempPart,i) = TempPairsPot(i,TempPart)
			else
				TempPairsPot(i,TempPart) = 0
				TempPairsPot(TempPart,i) = TempPairsPot(i,TempPart)
			end if
		end if
	end do
	TempPartPot = SUM(TempPairsPot(TempPart,:))
	PartPot = SUM(PairsPot(TempPart,:))

	call RANDOM_NUMBER(acceptation)
	Boltzmann = EXP(-beta*(TempPartPot-PartPot))
	if (acceptation.LT.Boltzmann) then
		Potential = Potential + TempPartPot - PartPot
		NbAcceptedPD=NbAcceptedPD+1
		PairsPot(:,:) = TempPairsPot
		PosParts(TempPart,:) = PosParts(TempPart,:)+Displacement-BoxL*ANINT((PosParts(TempPart,:)+Displacement)/BoxL)
	end if
END SUBROUTINE TRIAL_PARTICULE_DISPLACEMENT

SUBROUTINE RADIAL_DISTRIBUTION(NbPart,PosParts,BoxL,RadialDistribution,RadialIndex,Nb12Loop,NbPair)

	integer, intent(in) :: NbPart,Nb12Loop,NbPair
	real(8),intent(in) :: BoxL
	integer :: i=0,j=0
	integer, intent(inout) :: RadialIndex
	real(8), dimension(0:NbPart-1,0:2),intent(in) :: PosParts
	real(8), dimension(0:Nb12Loop*NbPair-1), intent(inout) :: RadialDistribution
	real(8), dimension(0:2) :: PosPeriodicParts_ij

	do i=0,NbPart-2
		do j=i+1,NbPart-1
			PosPeriodicParts_ij = PosParts(i,:)-PosParts(j,:)-BoxL*ANINT((PosParts(i,:)-PosParts(j,:))/BoxL)
			RadialDistribution(RadialIndex) = SQRT(SUM((PosPeriodicParts_ij)**2)) !normalization volume
			RadialIndex=RadialIndex+1
			k=k+1
		end do
	end do
END SUBROUTINE RADIAL_DISTRIBUTION

SUBROUTINE AMP_UPDATES(Amp,NbAccepted,NbCall)
	integer, intent(inout) :: NbAccepted, NbCall
	real(8), intent(inout) :: Amp
	if ((NbAccepted/REAL(NbCall)).LT.0.4) then !working for VC?
		Amp = Amp*0.9
	else if	((NbAccepted/REAL(NbCall)).GT.0.6) then
		Amp = Amp*1.1
	end if
	NbAccepted = 0
	NbCall = 0
END SUBROUTINE AMP_UPDATES

END MODULE SUBROUTINES

!----------MAIN----------!

PROGRAM LIQUID_POT
	USE SUBROUTINES
	IMPLICIT NONE
	integer :: Nb1Loop,Nb2Loop,NbPair,Nb12Loop,Nb3Loop
	integer :: i,j,k,l=0
	integer :: RadialIndex=0,NbPart,NbAcceptedPD=0,NbCallPD=0,TempRadialIndex=0
	real(8), parameter :: Kb=1.0
	real(8) :: AmpVC=0.2,beta,Temp,Random
	real(8), dimension(:,:), allocatable :: PairsPot, PosParts
	real(8), dimension(:), allocatable :: RadialDistribution, ListPotential,ListPressure
	real(8) :: Potential, BoxL, Rcut,AmpPD=0.1, MeanPotential, Pressure

	Nb1Loop=100
	Nb2Loop=100

	call READ_INPUTFILE(NbPart,BoxL,PosParts,Temp,Nb1Loop,Nb2Loop)


	Rcut = BoxL/2

	Nb3Loop=2*NbPart
	NbPair=((NbPart-1)*NbPart)/2
	Nb12Loop=Nb1Loop*Nb2Loop

	allocate(PairsPot(0:NbPart-1,0:NbPart-1))
	allocate(RadialDistribution(0:NbPair*Nb12Loop-1))
	allocate(ListPotential(0:Nb12Loop-1))
	allocate(ListPressure(0:Nb12Loop-1))


	beta = 1/(Kb*Temp)
	RadialIndex=0
	PairsPot=0
	RadialDistribution=0

  call POTENTIAL_INIT(NbPart,PosParts(:,:),PairsPot(:,:),BoxL,Potential,Rcut)
	call RANDOM_INIT()
	do i=0,Nb1Loop-1 !1Loop Updates of the displacement amplitude parameter
          Mu = 0
		do j=0,Nb2Loop-1 !2Loop Updates of the physical quantities
			do k=0,Nb3Loop-1 !3Loop Uncorrelation of physical observables
					call TRIAL_PARTICULE_DISPLACEMENT(NbPart,PosParts,PairsPot,BoxL,Potential,Rcut,&
							&AmpPD,beta,NbAcceptedPD,NbCallPD)
			TempRadialIndex = RadialIndex
			call RADIAL_DISTRIBUTION(NbPart,PosParts(:,:),BoxL,RadialDistribution,RadialIndex,Nb12Loop,NbPair)
			ListPotential(:,i*Nb2Loop+j) = Potential
			ListPressure(1,i*Nb2Loop+j) = (1/BoxL(1)**3)*SUM(24*(2*&
		          &(1.0/RadialDistribution(1,TempRadialIndex(1):RadialIndex(1)-1))**12&
		          &-(1.0/RadialDistribution(1,TempRadialIndex(1):RadialIndex(1)-1))**6))
			ListPressure(2,i*Nb2Loop+j) = (1/BoxL(2)**3)*SUM(24*(2*&
		          &(1.0/RadialDistribution(2,TempRadialIndex(2):RadialIndex(2)-1))**12&
		          &-(1.0/RadialDistribution(2,TempRadialIndex(2):RadialIndex(2)-1))**6))
		end do
		ListMu(:,i) = -(1/beta)*LOG(Mu/NbCallPE) !add thermal Brooglie wavelength
		print*,'PD',NbCallPD,NbAcceptedPD,AmpPD
		call AMP_UPDATES(AmpPD,NbAcceptedPD,NbCallPD)
		print*,'Loading...',(i*100)/Nb1Loop,'%'
	end do


	MeanPotential = SUM(ListPotential(:,Nb12Loop/2-1:),DIM=2)/(Nb12Loop/2)
  Pressure = (NbPart/BoxL**3)/beta+(1/6.0)*SUM(ListPressure(Nb12Loop/2-1:))/(Nb12Loop/2)&
					&+4*(NbPart/BoxL**3)**2*(((2.0)/(11*Rcut**11))-(1.0/(5*Rcut**5)))

	call WRITE_OUTPUTFILE(NbPart,BoxL,Nb1Loop,Nb12Loop,NbPair,PosParts,RadialDistribution,MeanPotential&
		&,Pressure,ListPotential)

	deallocate(PosParts)
	deallocate(PairsPot)
	deallocate(RadialDistribution)
	deallocate(ListPotential)
END PROGRAM LIQUID_POT
