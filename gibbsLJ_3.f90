MODULE SUBROUTINES
	IMPLICIT NONE
    logical :: ovrlap_box(2)
    integer :: Ntot,nvar,nVol,nswap,step,ncy,nupVol,b,a, nsub, acvm,adjstv,LC,nCP1,nupDens, nCycle
    integer, dimension(1:2) :: creat_box,remov_box,ipull_box,nup_box,adjst_box
    integer, dimension(1:2) :: N, N_0, tryin_box, nCP, acin_box, step_box
    real(8) :: ran,ran3(3),dvmax,ratiov,V12,V6,W12,W6,Xij,Yij,Zij,VolTotal
    real(8) :: XNEW,YNEW,ZNEW,PE12lrc,PE6lrc,W12lrc,W6lrc,beta,Temp,DensCP
    real(8), dimension(1:2) :: ratioc_box,accm_box,box_side,half_box_side,Dens,Vol,drmax
    real(8), dimension(1:2) :: PElrc12_box, Wlrc12_box, PElrc6_box, Wlrc6_box, rcut, Dens_0
    real(8), dimension(1:2) :: PE12_box, PE6_box, PEN_box, AVPE_box, PEMove12_box, PEMove6_box
    real(8), dimension(1:2) :: DV12_in_box, DV6_in_box, DV12_out_box, DV6_out_box, Vir12_box, Vir6_box
    real(8), dimension(1:2) :: VirMove12_box, VirMove6_box, DW12_in_box, DW6_in_box, DW12_out_box, DW6_out_box
    real(8), dimension(1:2) :: Pres_box, AVPres_box, AVDens_box, AVVol_box, AVN_box, xin_box, yin_box, zin_box
    real(8), dimension(1:2) :: accp_box, AVAcT_box, AVEx_box, ChemPot_box, ACChemPot_box
    real(8), dimension(1:2) :: ACChemPot2_box, AVChemPot_box, AVrcut
    real(8), dimension(:,:), allocatable :: X,Y,Z
	contains

!----------INITIALISATION|LECTURE DES DONNEES----------!

SUBROUTINE INIT()
	CHARACTER(len=32) :: NbPartString1,NbPartString2,TempString,DensityString1,DensityString2

	!USER INPUT
	CALL GET_COMMAND_ARGUMENT(1, TempString)
	CALL GET_COMMAND_ARGUMENT(2, NbPartString1)
	CALL GET_COMMAND_ARGUMENT(3, NbPartString2)
	CALL GET_COMMAND_ARGUMENT(4, DensityString1)
	CALL GET_COMMAND_ARGUMENT(5, DensityString2)

	IF (LEN_TRIM(TempString).EQ.0) then
		STOP 'No Temperature as argument'
	else
		TempString = TRIM(TempString)
		read(TempString,*) Temp
	end if

	IF ((LEN_TRIM(NbPartString1).EQ.0).OR.(LEN_TRIM(NbPartString2).EQ.0)) then
		STOP 'No Number of Particules as argument'
	else
		NbPartString1 = TRIM(NbPartString1)
		NbPartString2 = TRIM(NbPartString2)
		read(NbPartString1,*) N_0(1)
		read(NbPartString2,*) N_0(2)
		Ntot=N_0(1)+N_0(2)
	end if

	IF ((LEN_TRIM(DensityString1).EQ.0).OR.(LEN_TRIM(DensityString2).EQ.0)) then
		STOP 'No Density as argument'
	else
		DensityString1 = TRIM(DensityString1)
		DensityString2 = TRIM(DensityString2)
		read(DensityString1,*) Dens_0(1)
		read(DensityString2,*) Dens_0(2)
	end if

	allocate(X(1:2,0:NTot-1))
	allocate(Y(1:2,0:NTot-1))
	allocate(Z(1:2,0:NTot-1))
	X = 0
	Y = 0
	Z = 0
	!@@@@@@@@@@@@@@
	N_0=Ntot/2
	DensCP=0.3
	call DIAGRAM_INIT()
	print*,'N0',N_0,'Dens0',Dens_0
	!@@@@@@@@@@@@@@
	N = N_0
	Dens = Dens_0
	Vol = N/Dens
	box_side = Vol**(1.0/3.0)
	half_box_side = box_side/2
	drmax = box_side*0.02
	dvmax = MINVAL(Vol)*0.02

	beta = 1.0/Temp
	rcut = half_box_side
	PElrc12_box = (8.0*4.d0*DATAN(1.D0)*Dens*N)/(9*rcut**9)
	PElrc6_box = -(8.0*4.d0*DATAN(1.D0)*Dens*N)/(3*rcut**3)
	Wlrc12_box = (32.0*4.d0*DATAN(1.D0)*Dens*N)/(9*rcut**9)
	Wlrc6_box = -(16.0*4.d0*DATAN(1.D0)*Dens*N)/(3*rcut**3)
	b=1
	call initialConfiguration()
	call initialEnergy_and_Virial()
	call randomize()
	b=2
	call initialConfiguration()
	call initialEnergy_and_Virial()
	call randomize()
	PE12_box = PE12_box + PElrc12_box
	PE6_box = PE6_box + PElrc6_box
	Vir12_box = Vir12_box + Wlrc12_box
	Vir6_box = Vir6_box + Wlrc6_box
	AVDens_box = Dens
	AVN_box = N 
	AVPE_box = (PE12_box+PE6_box)/N 
	AVPres_box=Dens*Temp+(Vir12_box+Vir6_box)/Vol
	step=0
	step_box=0
	tryin_box=0
	nup_box=0
	nupVol=0
	creat_box=0
	ratioc_box=0
	ratiov=0
	accm_box=0
	acvm=0
	AVVol_box=0
	acin_box=0
	accm_box=0
	AVAcT_box=0
	AVEx_box=0
	ACChemPot_box=0
	ACChemPot2_box=0
	ChemPot_box=0
	nCP=0
	LC=0
	AVrcut=0
	nswap=Ntot
	ncy=99e4
	adjst_box=1e3
	adjstv=5e2
	nsub=200
	nVol=1
	nvar = Ntot+nVol+nswap
	VolTotal=Vol(1)+Vol(2)
	nCycle = 1e6
END SUBROUTINE INIT

SUBROUTINE DIAGRAM_INIT()
	real(8) :: DensPrime
	Dens_0(1) = (1.0/15)*Temp-(1.0/20)
	Dens_0(2) = -(6.0/10)*Temp+(27.0/20)
	DensPrime = 1.0*Ntot/((N_0(1)/Dens_0(1))+(N_0(2)/Dens_0(2)))
	do while (DensPrime.LT.DensCP)
		N_0(1)=N_0(1)-1
		N_0(2)=Ntot-N_0(1)
		DensPrime = 1.0*Ntot/((N_0(1)/Dens_0(1))+(N_0(2)/Dens_0(2)))
	end do
END SUBROUTINE DIAGRAM_INIT

!----------BOUCLE MONTE CARLO----------!

SUBROUTINE MAIN()
    integer :: i,j
	do i=0, nCycle
		step=step+1
		do j=0,nvar-1
			call RANDOM_NUMBER(ran)
			if (Ran .LT. 1.0*N(1)/nvar) then
				b=1
				call TRIAL_PARTICULE_DISPLACEMENT()
			else if (Ran .LT. 1.0*Ntot/nvar) then
				b=2
				call TRIAL_PARTICULE_DISPLACEMENT()
			else if (Ran .LT. 1.0*(NTot+nVol)/nvar) then
				call TRIAL_VOLUME_CHANGE()
			else if (Ran .LT. (NTot+nVol+0.5*nswap)/nvar) then
				b=1
				a=2
				call TRIAL_PARTICULE_EXCHANGE()
			else
				b=2
				a=1
				call TRIAL_PARTICULE_EXCHANGE()
			end if
		end do
		if (step.GT.ncy) then
            call statistics()
            print*,AVPE_box,AVN_box,AVDens_box,AVVol_box
            print*,AVPres_box,AVChemPot_box,i
		end if
	end do
END SUBROUTINE MAIN

SUBROUTINE potential(RijSQ)
	real(8), intent(in) :: RijSQ
	real(8) :: ALJ2,ALJ6,ALJ12
	ALJ2 = 1.0/RijSQ
	ALJ6  = ALJ2*ALJ2*ALJ2
	ALJ12 = ALJ6*ALJ6
	V12 = V12 +ALJ12
	V6  = V6  -ALJ6
	W12 = W12 +ALJ12
	W6  = W6  -ALJ6*0.5
END SUBROUTINE potential

SUBROUTINE minImage(side,half_side)
	real(8), intent(in) :: side,half_side
	if (Xij .GT. half_side) Xij = Xij - side
	if (Yij .GT. half_side) Yij = Yij - side
	if (Zij .GT. half_side) Zij = Zij - side
	if (Xij .LT. -half_side) Xij = Xij + side
	if (Yij .LT. -half_side) Yij = Yij + side
	if (Zij .LT. -half_side) Zij = Zij + side
END SUBROUTINE minImage

SUBROUTINE boundary(side,half_side)
	real(8), intent(in) :: side,half_side
	if (XNEW .GT. half_side) XNEW = XNEW - side
	if (YNEW .GT. half_side) YNEW = YNEW - side
	if (ZNEW .GT. half_side) ZNEW = ZNEW - side
	if (XNEW .LT. -half_side) XNEW = XNEW + side
	if (YNEW .LT. -half_side) YNEW = YNEW + side
	if (ZNEW .LT. -half_side) ZNEW = ZNEW + side
END SUBROUTINE boundary

SUBROUTINE longRangeCorrections(Volume,rcut_)
	real(8), intent(in) :: Volume, rcut_
	PE12lrc = (8.0*4.d0*DATAN(1.D0)/Volume)/(9*rcut_**9)
	PE6lrc = -(8.0*4.d0*DATAN(1.D0)/Volume)/(3*rcut_**3)
	W12lrc = (32.0*4.d0*DATAN(1.D0)/Volume)/(9*rcut_**9)
	W6lrc = -(16.0*4.d0*DATAN(1.D0)/Volume)/(3*rcut_**3)
END SUBROUTINE longRangeCorrections

SUBROUTINE initialConfiguration()
	integer :: nucpd,i,lx,ly,lz,j
	real(8) :: unit_cell_side
	real(8), dimension(0:3) :: xc = (/0.0,0.0,0.5,0.5/)
	real(8), dimension(0:3) :: yc = (/0.0,0.5,0.5,0.0/)
	real(8), dimension(0:3) :: zc = (/0.0,0.5,0.0,0.5/)
	nucpd=0
	if (N(b) .LT. 4+1) nucpd=1
	if ((N(b) .GT. 4)  .AND. (N(b) .LT. 32+1)) nucpd=2
	if ((N(b) .GT. 32)  .AND. (N(b) .LT. 108+1)) nucpd=3
	if ((N(b) .GT. 108) .AND. (N(b) .LT. 256+1)) nucpd=4
	if ((N(b) .GT. 256) .AND. (N(b) .LT. 500+1)) nucpd=5
	if ((N(b).GT.500) .AND. (N(b) .LT. 864+1)) nucpd=6
	if (N(b) .GT. 864) STOP
	unit_cell_side = box_side(b)/nucpd
	i=0
	do lx=1,nucpd
		do ly=1,nucpd
			do lz=1,nucpd
				do j=0,3
					if (i.LT.N(b)) then
				    	X(b,i) =(lx - 0.75 + xc(j))*unit_cell_side - half_box_side(b)
				    	Y(b,i) =(ly - 0.75 + yc(j))*unit_cell_side - half_box_side(b)
				    	Z(b,i) =(lz - 0.75 + zc(j))*unit_cell_side - half_box_side(b)
				    	i=i+1
				    end if
				end do 
			end do 
		end do 
	end do
END SUBROUTINE initialConfiguration

SUBROUTINE randomize()
	real(8) :: PE12OLD,PE6OLD,W12OLD,W6OLD,PE12NEW,PE6NEW
	real(8) :: W12NEW,W6NEW,delPE12,delPE6,delW12,delW6,delPEB
	integer :: m,i
	do m=0, 50
		do i=0, N(b)-1
			call Energy_Box(X(b,i),Y(b,i),Z(b,i),i)
    		PE12OLD = PEMove12_box(b)
    		PE6OLD  = PEMove6_box(b)
    		W12OLD  = VirMove12_box(b)
    		W6OLD   = VirMove6_box(b)
    		call RANDOM_NUMBER(Ran3)
    		XNEW = X(b,i) + (2.0*ran3(1)-1) * drmax(b)
    		YNEW = Y(b,i) + (2.0*ran3(2)-1) * drmax(b)
    		ZNEW = Z(b,i) + (2.0*ran3(3)-1) * drmax(b)
    		call boundary(box_side(b),half_box_side(b))
    		call Energy_Box(XNEW,YNEW,ZNEW,i)
    		PE12NEW = PEMove12_box(b)
    		PE6NEW  = PEMove6_box(b)
    		W12NEW  = VirMove12_box(b)
    		W6NEW   = VirMove6_box(b)
    		delPE12 = PE12NEW - PE12OLD
    		delPE6  = PE6NEW - PE6OLD
    		delW12  = W12NEW - W12OLD
    		delW6   = W6NEW - W6OLD
    		delPEB  = beta * (delPE12 + delPE6)
    		call RANDOM_NUMBER(Ran)
    		if ((delPEB.LT.0.0).OR.(EXP(-delPEB).GT.ran)) then
	    		PE12_box(b)  = PE12_box(b)+delPE12
	    		PE6_box(b)   = PE6_box(b)+delPE6
	    		Vir12_box(b) = Vir12_box(b)+delW12
	    		Vir6_box(b)  = Vir6_box(b)+delW6
        		X(b,i) = XNEW
        		Y(b,i) = YNEW
        		Z(b,i) = ZNEW
    		end if
		end do
	end do
END SUBROUTINE randomize

SUBROUTINE initialEnergy_and_Virial()
	real(8) :: rcutSQ,RijSQ
	integer :: i,j
	rcutSQ = rcut(b)*rcut(b)
	V12 =0
	V6 =0
	W12 =0
	W6 =0
	do i=0,N(b)-2
		do j=i+1,N(b)-1
    		Xij = X(b,i) - X(b,j)
    		Yij = Y(b,i) - Y(b,j)
    		Zij = Z(b,i) - Z(b,j)
    		call minImage(box_side(b),half_box_side(b))
    		RijSQ = Xij * Xij + Yij * Yij + Zij * Zij
    		if (RijSQ.LT.rcutSQ) call potential(RijSQ)
    	end do
	end do
	PE12_box(b)  = 4.0 * V12
	PE6_box(b)  = 4.0 * V6
	Vir12_box(b) = 48.0 * W12/3.0
	Vir6_box(b) = 48.0 * W6/3.0
END SUBROUTINE initialEnergy_and_Virial

!----------ACTION 1 DEPLACEMENT D'UNE PARTICULE----------!

SUBROUTINE TRIAL_PARTICULE_DISPLACEMENT()
	integer :: i
	real(8) :: PE12OLD,PE6OLD,W12OLD,W6OLD,PE12NEW,PE6NEW
	real(8) :: W12NEW,W6NEW,delPE12,delPE6,delW12,delW6,delPEB
	if (N(b).GT.1) then
		step_box(b) = step_box(b) +1
		call RANDOM_NUMBER(ran)
		i = INT(ran*N(b))
		call Energy_Box(X(b,i),Y(b,i),Z(b,i),i)
		PE12OLD = PEMove12_box(b)
		PE6OLD  = PEMove6_box(b)
		W12OLD  = VirMove12_box(b)
		W6OLD   = VirMove6_box(b)
		call RANDOM_NUMBER(Ran3)
		XNEW = X(b,i) + (2.0*ran3(1)-1) * drmax(b)
		YNEW = Y(b,i) + (2.0*ran3(2)-1) * drmax(b)
		ZNEW = Z(b,i) + (2.0*ran3(3)-1) * drmax(b)
		call boundary(box_side(b),half_box_side(b))
		call Energy_Box(XNEW,YNEW,ZNEW,i)
		PE12NEW = PEMove12_box(b)
		PE6NEW  = PEMove6_box(b)
		W12NEW  = VirMove12_box(b)
		W6NEW   = VirMove6_box(b)
		delPE12 = PE12NEW - PE12OLD
		delPE6  = PE6NEW - PE6OLD
		delW12  = W12NEW - W12OLD
		delW6   = W6NEW - W6OLD
		delPEB  = beta * (delPE12 + delPE6)
		call RANDOM_NUMBER(Ran)
		if ((delPEB.LT.0.0).OR.(EXP(-delPEB).GT.ran)) then
			accm_box(b) = accm_box(b)+1
    		PE12_box(b)  = PE12_box(b)+delPE12
    		PE6_box(b)   = PE6_box(b)+delPE6
    		Vir12_box(b) = Vir12_box(b)+delW12
    		Vir6_box(b)  = Vir6_box(b)+delW6
    		X(b,i) = XNEW
    		Y(b,i) = YNEW
    		Z(b,i) = ZNEW
    		PEN_box(b) = (PE12_box(b)+PE6_box(b))/N(b)
    		Pres_box(b) = Dens(b)*Temp + (Vir12_box(b)+Vir6_box(b))/Vol(b)
		end if
		nup_box(b) = nup_box(b)+1
		if (MODULO(nup_box(b),adjst_box(b)).EQ.0) then
			ratioc_box(b) = 1.0*accm_box(b)/adjst_box(b)
			if (ratioc_box(b) .GT. 0.5) then
				drmax(b) = drmax(b) * 1.05
			else
				drmax(b) = drmax(b) * 0.95
			end if
			if (drmax(b) > half_box_side(b)) drmax(b)=half_box_side(b)
			accm_box(b) = 0.0
		end if
	else
		PEN_box(b) = 0.0
		Pres_box(b) = Dens(b)*Temp
	end if
END SUBROUTINE TRIAL_PARTICULE_DISPLACEMENT

!----------ACTION 2 ECHANGE DE VOLUME----------!

SUBROUTINE TRIAL_VOLUME_CHANGE()
	real(8) :: delVol, delvol_fact, delbetaPEVol
	real(8), dimension(2) :: Vol_new,box_sideNew,ratbox,rrbox,PE12OLD_box,PE6OLD_box, Vol_OLD, Vir12OLD_box
	real(8), dimension(2) :: delPE_box, box_side_OLD, Vir6OLD_box
	real(8), dimension(1:2,0:NTot-1) :: XOLD,YOLD,ZOLD
	
	!delr = 0.9/nrho
	call RANDOM_NUMBER(ran)
	Vol_new(1) = Vol(1) + (2.0*ran-1)*dvmax
	Vol_new(2) = VolTotal - Vol_new(1)
	
	if ((Vol_new(1).GT.0.0).AND.(Vol_new(2).GT.0.0)) then
		box_sideNew = Vol_new**(1.0/3.0)
		ratbox = box_side/box_sideNew
		rrbox = 1.0/ratbox
        PE12OLD_box = PE12_box
        PE6OLD_box =  PE6_box
        Vir12OLD_box = Vir12_box
        Vir6OLD_box = Vir6_box
        Vol_OLD = Vol
        box_side_OLD = box_side
        
        XOLD = X
        YOLD = Y
        ZOLD = Z
        
        X(1,:) = X(1,:)*rrbox(1)
        Y(1,:) = Y(1,:)*rrbox(1)
        Z(1,:) = Z(1,:)*rrbox(1)
        X(2,:) = X(2,:)*rrbox(2)
        Y(2,:) = Y(2,:)*rrbox(2)
        Z(2,:) = Z(2,:)*rrbox(2)

        box_side = box_sideNew
        half_box_side = box_side/2.0
        rcut = half_box_side
        vol = Vol_new
        Dens = N/Vol
        PElrc12_box = (8.0*4.d0*DATAN(1.D0)*Dens*N)/(9*rcut**9)
        PElrc6_box = -(8.0*4.d0*DATAN(1.D0)*Dens*N)/(3*rcut**3)
        Wlrc12_box = (32.0*4.d0*DATAN(1.D0)*Dens*N)/(9*rcut**9)
        Wlrc6_box = -(16.0*4.d0*DATAN(1.D0)*Dens*N)/(3*rcut**3)
        b=1
        call initialEnergy_and_Virial()
        b=2
        call initialEnergy_and_Virial()
        PE12_box = PE12_box + PElrc12_box
        PE6_box = PE6_box + PElrc6_box
        Vir12_box = Vir12_box + Wlrc12_box
        Vir6_box = Vir6_box + Wlrc6_box
        delPE_box = PE12_box + PE6_box - PE12OLD_box - PE6OLD_box
        delVol = Vol_New(1) - Vol(1)
        delVol_fact = -N(1)*Temp*LOG(Vol(1)/Vol_OLD(1))-N(2)*Temp*LOG(Vol(2)/Vol_OLD(2))
        delbetaPEVol = beta*(delPE_box(1)+delPE_box(2)+delVol_fact)
		call RANDOM_NUMBER(ran)
		if ((delbetaPEVol.LT.0.0).OR.(EXP(-delbetaPEVol).GT.ran)) then
			acvm = acvm +1
        else
            PE12_box = PE12OLD_box
            PE6_box =  PE6OLD_box
            Vir12_box = Vir12OLD_box
            Vir6_box = Vir6OLD_box
            Vol = Vol_OLD
            box_side = box_side_OLD
    		half_box_side = box_side/2.0
    		Rcut = half_box_side
    		X = XOLD
    		Y = YOLD
    		Z = ZOLD
		end if
		Dens = N/Vol
		nupDens = nupDens +1
		if (N(1).GT.1) then
    		PEN_box(1) = (PE12_box(1)+PE6_box(1))/N(1)
    		Pres_box(1) = Dens(1)*Temp + (Vir12_box(1)+Vir6_box(1))/Vol(1)
    	else
			PEN_box(1) = 0.0
			Pres_box(1) = Dens(1)*Temp
    	end if
		if (N(2).GT.1) then
    		PEN_box(2) = (PE12_box(2)+PE6_box(2))/N(2)
    		Pres_box(2) = Dens(2)*Temp + (Vir12_box(2)+Vir6_box(2))/Vol(2)
    	else
			PEN_box(2) = 0.0
			Pres_box(2) = Dens(2)*Temp
    	end if
	end if
	nupVol = nupVol+1
	if (MODULO(nupVol,adjstv).EQ.0) then
		ratiov = 1.0*acvm/adjstv
		if (ratiov .GT. 0.5) then
			dvmax = dvmax * 1.05
		else
			dvmax = dvmax * 0.95
		end if
		acvm = 0.0
	end if
END SUBROUTINE TRIAL_VOLUME_CHANGE

!----------ACTION 3 ECHANGE DE PARTICULES----------!

SUBROUTINE TRIAL_PARTICULE_EXCHANGE()
	real(8) :: delInOut
	real(8), dimension(2) :: delPE_box
	if (N(b).LT.Ntot) then
		tryin_box(b) = tryin_box(b)+1
		call particuleIn()
		if (.NOT.(ovrlap_box(b))) then
			call particuleOut()
			acin_box(b) = acin_box(b)+1
			delPE_box(b) = DV12_in_box(b)+DV6_in_box(b)
			delPE_box(a) = DV12_out_box(a) + DV6_out_box(a)
			accp_box(b) = accp_box(b) + EXP(-beta*delPE_box(b))
			delInOut = beta*(delPE_box(b)+delPE_box(a)+Temp*LOG((Vol(a)*(N(b)+1))/(Vol(b)*N(a))))
			call RANDOM_NUMBER(ran)
			if ((delInOut.LT.0.0).OR.(EXP(-delInOut).GT.ran)) then
				creat_box(b) = creat_box(b) +1
	    		PE12_box(b)  = PE12_box(b)  +DV12_in_box(b)
	    		PE6_box(b)   = PE6_box(b)   +DV6_in_box(b)
	    		Vir12_box(b) = Vir12_box(b) +DW12_in_box(b)
	    		Vir6_box(b)  = Vir6_box(b)  +DW6_in_box(b)
	    		X(b,N(b)) = xin_box(b)
	    		Y(b,N(b)) = yin_box(b)
	    		Z(b,N(b)) = zin_box(b)
	    		N(b)=N(b)+1
	    		remov_box(a) = remov_box(a) +1
	    		PE12_box(a)  = PE12_box(a)  +DV12_out_box(a)
	    		PE6_box(a)   = PE6_box(a)   +DV6_out_box(a)
	    		Vir12_box(a) = Vir12_box(a) +DW12_out_box(a)
	    		Vir6_box(a)  = Vir6_box(a)  +DW6_out_box(a)
	    		X(a,ipull_box(a)) = X(a,N(a)-1)
	    		Y(a,ipull_box(a)) = Y(a,N(a)-1)
	    		Z(a,ipull_box(a)) = Z(a,N(a)-1)
	    		N(a)=N(a)-1
    		end if
    	end if
    	Dens = N/Vol 
    	nupDens = nupDens+1
		if (N(1).GT.1) then
    		PEN_box(1) = (PE12_box(1)+PE6_box(1))/N(1)
    		Pres_box(1) = Dens(1)*Temp + (Vir12_box(1)+Vir6_box(1))/Vol(1)
    	else
			PEN_box(1) = 0.0
			Pres_box(1) = Dens(1)*Temp
    	end if
		if (N(2).GT.1) then
    		PEN_box(2) = (PE12_box(2)+PE6_box(2))/N(2)
    		Pres_box(2) = Dens(2)*Temp + (Vir12_box(2)+Vir6_box(2))/Vol(2)
    	else
			PEN_box(2) = 0.0
			Pres_box(2) = Dens(2)*Temp
    	end if
    else
    	tryin_box(b)=tryin_box(b)+1
    	call particuleIn()
    	if (.NOT.(ovrlap_box(b))) then
    		acin_box(b) = acin_box(b) +1
    		delPE_box(b) = DV12_in_box(b)+DV6_in_box(b)
    		accp_box(b) = accp_box(b) + EXP(-beta*delPE_box(b))
        end if
    end if
END SUBROUTINE TRIAL_PARTICULE_EXCHANGE

SUBROUTINE Energy_Box(XI,YI,ZI,i)
	real(8) :: rcutSQ,RijSQ
	integer, intent(in) :: i
	integer :: j
	real(8), intent(in) :: XI,YI,ZI
	rcutSQ = rcut(b)*rcut(b)
	V12 =0
	V6 =0
	W12 =0
	W6 =0
	do j=0,N(b)-1
		if (i .NE. j) then
    		Xij = XI - X(b,j)
    		Yij = YI - Y(b,j)
    		Zij = ZI - Z(b,j)
    		call minImage(box_side(b),half_box_side(b))
    		RijSQ = Xij * Xij + Yij * Yij + Zij * Zij
    		if (RijSQ.LT.rcutSQ) then
    			call potential(RijSQ)
    			!if (RF)
    		end if
    	end if
	end do
	PEMove12_box(b)  = 4.0 * V12
	PEMove6_box(b)  = 4.0 * V6
	VirMove12_box(b) = 48.0 * W12/3.0
	VirMove6_box(b) = 48.0 * W6/3.0
END SUBROUTINE Energy_Box

SUBROUTINE particuleIn()
	call RANDOM_NUMBER(ran3)
	xin_box(b) = ran3(1)*box_side(b)-half_box_side(b)
	yin_box(b) = ran3(2)*box_side(b)-half_box_side(b)
	zin_box(b) = ran3(3)*box_side(b)-half_box_side(b)
	if (N(b).GT.0) then
		call potin(xin_box(b),yin_box(b),zin_box(b))
	else
	    DV12_in_box(b) = 0.0
	    DV6_in_box(b)  = 0.0
	    DW12_in_box(b) = 0.0 
	    DW6_in_box(b)  = 0.0
	    ovrlap_box(b) = .false.
	end if
END SUBROUTINE particuleIn

SUBROUTINE potin(xgrd,ygrd,zgrd)
	real(8), intent(in) :: xgrd,ygrd,zgrd
	real(8) :: rcutSQ,RijSQ,rminSQD
	integer :: j
	ovrlap_box(b)=.false.
	rcutSQ = rcut(b)*rcut(b)
	rminSQD = 0.8*0.8
	!delr = rcut(b)/nrdf
	V12 =0
	V6 =0
	W12 =0
	W6 =0
	do j=0,N(b)-1
		Xij = xgrd - X(b,j)
		Yij = ygrd - Y(b,j)
		Zij = zgrd - Z(b,j)
    	call minImage(box_side(b),half_box_side(b))
    	RijSQ = Xij * Xij + Yij * Yij + Zij * Zij
		if (RijSQ.LT.rminSQD) then
            ovrlap_box(b)=.true.
            return
		else
			if (RijSQ.LT.rcutSQ) call potential(RijSQ)
		end if
	end do
	call longRangeCorrections(Vol(b),rcut(b))
	DV12_in_box(b)  = ((4.0 * V12) + (2*N(b)+1)* PE12lrc)
	DV6_in_box(b)   = ((4.0 * V6)  + (2*N(b)+1)* PE6lrc)
	DW12_in_box(b)  = ((48.0 * W12/3.0) + (2*N(b)+1)* W12lrc)
	DW6_in_box(b)   = ((48.0 * W6 / 3.0)+ (2*N(b)+1)* W6lrc)
END SUBROUTINE potin

SUBROUTINE particuleOut()
	call RANDOM_NUMBER(ran)
	ipull_box(a) = INT(N(a)*ran)
	if (N(a).GT.1) then
		call potout(ipull_box(a))
	else
	    DV12_out_box(a) = 0.0
	    DV6_out_box(a)  = 0.0
	    DW12_out_box(a) = 0.0 
	    DW6_out_box(a)  = 0.0
	end if
END SUBROUTINE particuleOut

SUBROUTINE potout(ipullo)
	real(8) :: rcutSQ,RijSQ,rx,ry,rz
	integer, intent(in) :: ipullo
	integer :: j
	rcutSQ = rcut(a)*rcut(a)
	!delr = rcut(b)/nrdf
	V12 =0
	V6 =0
	W12 =0
	W6 =0
	rx = X(a,ipullo)
	ry = Y(a,ipullo)
	rz = Z(a,ipullo)
	do j=0,N(a)-1
		if (j.NE.ipullo) then
			Xij = rx - X(a,j)
			Yij = ry - Y(a,j)
			Zij = rz - Z(a,j)
	    	call minImage(box_side(a),half_box_side(a))
	    	RijSQ = Xij * Xij + Yij * Yij + Zij * Zij
			if (RijSQ.LT.rcutSQ) call potential(RijSQ)
		end if
	end do
	call longRangeCorrections(Vol(a),rcut(a))
	DV12_out_box(a)  = -((4.0 * V12) + (2*N(a)-1)* PE12lrc)
	DV6_out_box(a)   = -((4.0 * V6)  + (2*N(a)-1)* PE6lrc)
	DW12_out_box(a)  = -((48.0 * W12/3.0) + (2*N(a)-1)* W12lrc)
	DW6_out_box(a)   = -((48.0 * W6 / 3.0)+ (2*N(a)-1)* W6lrc)
END SUBROUTINE potout

SUBROUTINE statistics()
	integer :: LC1
	LC = LC+1
	LC1 = LC-1
	AVPE_box=(AVPE_box*LC1+PEN_box)/LC
	AVDens_box=(AVDens_box*LC1+Dens)/LC
	AVN_box=(AVN_box*LC1+N)/LC
	AVPres_box=(AVPres_box*LC1+Pres_box)/LC
	AVVol_box=(AVVol_box*LC1+Vol)/LC
	AVrcut=(AVrcut*LC1+rcut)/LC
	AVAcT_box=(1.0*acin_box/tryin_box)* 100
	AVEx_box=(1.0*creat_box/tryin_box)* 100
	if ((accp_box(1) .GT. 0.0) .AND. (AVDens_box(1) .GT. 0.0)) then
	  ChemPot_box(1)=Temp*(LOG(AVDens_box(1))-LOG(1.0*accp_box(1)/tryin_box(1)))
	end if
	if ((accp_box(2) .GT. 0.0) .AND. (AVDens_box(2) .GT. 0.0)) then
	  ChemPot_box(2)=Temp*(LOG(AVDens_box(2))-LOG(1.0*accp_box(2)/tryin_box(2)))
	end if
	nCP1 = nCP1 + 1;
	ACChemPot_box = ACChemPot_box +ChemPot_box
	ACChemPot2_box = ACChemPot2_box +ChemPot_box * ChemPot_box
	AVChemPot_box = ACChemPot_box/nCP1
	!FLChemPot_box = SQRT(ABS((ACChemPot2_box/nCP1)-(AVChemPot_box*AVChemPot_box)))
	if (LC.EQ.1) then
		open(34, file="PEEvol.txt",access='sequential', form='formatted', status="unknown", action="write")
		open(35, file="VolEvol.txt",access='sequential', form='formatted', status="unknown", action="write")
		open(36, file="NEvol.txt",access='sequential', form='formatted', status="unknown", action="write")
	end if
	write(34,*) PEN_box
	write(35,*) Vol/VolTotal
	write(36,*) 1.0*N/Ntot
		
END SUBROUTINE statistics

SUBROUTINE RANDOM_INIT()
	integer :: k
	integer, dimension(0:7) :: values
	integer, dimension(:), allocatable :: seed
	call date_and_time(values=values)
	call RANDOM_SEED(size=k)
	allocate(seed(1:k))
	seed(:) = values(7)
	call RANDOM_SEED(put=seed)
END SUBROUTINE RANDOM_INIT

END MODULE SUBROUTINES

!----------LANCEMENT DES SUBROUTINES----------!

PROGRAM LIQUID_POT

	USE SUBROUTINES
	IMPLICIT NONE

	call RANDOM_INIT()
	call INIT()
	call MAIN()

END PROGRAM LIQUID_POT
