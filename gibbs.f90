!----------SUBROUTINES----------!

MODULE SUBROUTINES
	contains
SUBROUTINE READ_INPUTFILE(NbPartTot,NbPart,BoxL,PosParts,Temp,OutputFilePath,Nb1Loop,Nb2Loop)
	integer, dimension(1:2), intent(out) :: NbPart
	integer, intent(out) :: NbPartTot
	integer, intent(in) :: Nb1Loop,Nb2Loop
	integer :: i,j
	real(8), dimension(1:2), intent(out) :: BoxL
	real(8), dimension(1:2) :: Density, Ratio
	real(8), intent(out) :: Temp
	real(8), dimension(:,:,:), allocatable, intent(out) :: PosParts
	CHARACTER(len=32) :: InputFilePath,TempString,DensityString1,DensityString2
	CHARACTER(len=32), intent(out) :: OutputFilePath

  CALL GET_COMMAND_ARGUMENT(1, InputFilePath)
	CALL GET_COMMAND_ARGUMENT(2, OutputFilePath)
	CALL GET_COMMAND_ARGUMENT(3, TempString)
	CALL GET_COMMAND_ARGUMENT(4, DensityString1)
	CALL GET_COMMAND_ARGUMENT(5, DensityString2)


	IF (LEN_TRIM(InputFilePath) == 0) then
		STOP 'No input file as argument'
	else
		open(12, file=TRIM(InputFilePath),access='sequential', form='formatted', status="old", action="read")
		read(12,*) NbPartTot,NbPart(1),NbPart(2)
		read(12,*) BoxL(1),BoxL(2)
		allocate(PosParts(1:2,0:NbPartTot-1,0:2))
		do i=0,NbPart(1)-1
			read(12,113) WhichParts,PosParts(1,i,:)
			113 format (A3,3ES15.7) !implement argument
		end do
		do i=0,NbPart(2)-1
			read(12,114) WhichParts,PosParts(2,i,:)
			114 format (A3,3ES15.7) !implement argument
		end do
		close(12)
	end if

	IF (LEN_TRIM(OutputFilePath) == 0) then
		STOP 'No output file as argument'
	else
		call SYSTEM('mkdir '//TRIM(OutputFilePath)) !!!
	end if

	IF (LEN_TRIM(TempString) == 0) then
		Temp = 1.13
	else
		TempString = TRIM(TempString)
		read(TempString,*) Temp
	end if

	IF ((LEN_TRIM(DensityString1) == 0).OR.(LEN_TRIM(DensityString2) == 0)) then
	else
		DensityString1 = TRIM(DensityString1)
		read(DensityString1,*) Density(1)
		DensityString2 = TRIM(DensityString2)
		read(DensityString2,*) Density(2)
		Ratio = (NbPart/Density)**(1/3.0)/BoxL
		BoxL = (NbPart/Density)**(1/3.0)
		PosParts(1,:,:) = Ratio(1)*PosParts(1,:,:)
		PosParts(2,:,:) = Ratio(2)*PosParts(2,:,:)
	end if

	open(12, file=TRIM(OutputFilePath)//"/simulation.txt",access='sequential', form='formatted', status="unknown", action="write")
	write(12,*) 'Number of particules',NbPart(1),NbPart(2)
	write(12,*) 'Volumes',BoxL(1)**3,BoxL(2)**3
	write(12,*) 'Temperature',Temp
	write(12,*) 'Number of loops',Nb1Loop,Nb2Loop

	close(12)
END SUBROUTINE READ_INPUTFILE

SUBROUTINE WRITE_OUTPUTFILE(NbPartTot,NbPart,BoxL,Nb1Loop,Nb12Loop,NbPair,PosParts,RadialDistribution,MeanPotential&
		&,Pressure,MeanVolume,MeanEnthalpy,ListBoxL,ListNbPart,VolumeTot,ListPotential,ListMu,OutputFilePath,MeanNbPart,MeanMu)

	integer, intent(in) :: NbPartTot,Nb12Loop,NbPair
	integer, dimension(1:2), intent(in) :: NbPart
	integer :: i
	integer, dimension(1:2,0:Nb12Loop-1) ::ListNbPart
	real(8), dimension(1:2), intent(in) :: MeanPotential,BoxL,Pressure,MeanVolume,MeanEnthalpy,MeanNbPart,MeanMu
	real(8), dimension(1:2,0:NbPartTot-1,0:2),intent(in) :: PosParts
	real(8), dimension(1:2,0:Nb12Loop*NbPair-1), intent(in) :: RadialDistribution
	real(8), dimension(1:2,0:Nb12Loop-1) :: ListBoxL, ListPotential
	real(8), dimension(1:2,Nb1Loop-1) :: ListMu
	real(8), intent(in) :: VolumeTot
	CHARACTER(len=3) :: WhichParts = 'Ar '
	CHARACTER(len=32) :: OutputFilePath

	open(12, file=TRIM(OutputFilePath)//"/VolumeEvol.dat", status="unknown", access="stream", action="write")
	write(12) ListBoxL**3/VolumeTot
	close(12)

	open(12, file=TRIM(OutputFilePath)//"/NbPartEvol.dat", status="unknown", access="stream", action="write")
	write(12) ListNbPart/Real(NbPartTot)
	close(12)

	open(12, file=TRIM(OutputFilePath)//"/PotentialEvol.dat", status="unknown", access="stream", action="write")
	write(12) ListPotential
	close(12)

	open(12, file=TRIM(OutputFilePath)//"/MuEvol.dat", status="unknown", access="stream", action="write")
	write(12) ListMu
	close(12)

	! open(12, file=TRIM(OutputFilePath)//"/RadialDistri.dat", status="unknown", access="stream", action="write")
	! write(12) RadialDistribution
	! close(12)

	open(12, file=TRIM(OutputFilePath)//"/simulation.txt",access='append', form='formatted', status="old", action="write")
	write(12,*) 'Potential         ',MeanPotential
	write(12,*) 'Enthalpy          ',MeanEnthalpy
	write(12,*) 'Volume            ',MeanVolume
	write(12,*) 'NbPart            ',MeanNbPart
	write(12,*) 'Pressure          ',Pressure
	write(12,*) 'ChimicalPot       ',MeanMu
	close(12)

	open(12, file=TRIM(OutputFilePath)//"/gibbs.of",access='sequential', form='formatted', status="unknown", action="write")
	write(12,*) SUM(NbPart),NbPart(1),NbPart(2)
	write(12,*) BoxL(1),BoxL(2)
	do i=0,NbPart(1)-1
		write(12,113) WhichParts,PosParts(1,i,:)
		113 format (A3,3ES15.7)
	end do
	do i=0,NbPart(2)-1
		write(12,114) WhichParts,PosParts(2,i,:)
		114 format (A3,3ES15.7)
	end do
	close(12)
END SUBROUTINE WRITE_OUTPUTFILE

SUBROUTINE POTENTIAL_INIT(NbPartTot,NbPart,PosParts,PairsPot,BoxL,Potential,Rcut,epsi,sigma)

	real(8), intent(in) :: epsi, sigma, BoxL, Rcut
	real(8), intent(out) :: Potential
	integer, intent(in) :: NbPart,NbPartTot
	integer :: i=0,j=0
	real(8), dimension(0:NbPartTot-1,0:2),intent(in) :: PosParts
	real(8), dimension(0:2) :: PosPeriodicParts_ij
	real(8), dimension(0:NbPartTot-1,0:NbPartTot-1),intent(out) :: PairsPot
	real(8) :: r_ij
	PairsPot = 0
	Potential = 0

	do i=0,NbPart-2
		do j=i+1,NbPart-1
			PosPeriodicParts_ij = PosParts(i,:)-PosParts(j,:)-BoxL*ANINT((PosParts(i,:)-PosParts(j,:))/BoxL)
			r_ij = SQRT(SUM((PosPeriodicParts_ij)**2))
			if (r_ij.LT.Rcut) then !Potential truncation?
				PairsPot(i,j) = (sigma/r_ij)**12-(sigma/r_ij)**6
				PairsPot(j,i) = PairsPot(i,j)
				Potential = Potential + PairsPot(i,j)
			else
				PairsPot(i,j) = 0
				PairsPot(j,i) = PairsPot(i,j)
			end if
		end do
	end do

	PairsPot = 4*epsi*PairsPot
	Potential = 4*epsi*Potential
END SUBROUTINE POTENTIAL_INIT

SUBROUTINE RANDOM_INIT()

	integer :: k
	integer, dimension(0:7) :: values
	integer, dimension(:), allocatable :: seed
	call date_and_time(values=values)
	call RANDOM_SEED(size=k)
	allocate(seed(1:k))
	seed(:) = values(7)
	call RANDOM_SEED(put=seed)
END SUBROUTINE RANDOM_INIT

SUBROUTINE TRIAL_PARTICULE_DISPLACEMENT(NbPartTot,NbPart,PosParts,PairsPot,BoxL,Potential,Rcut,&
		&epsi,sigma,AmpPD,beta,NbAcceptedPD,NbCallPD)
	integer :: TempPart, i=0,b
	integer, intent(in) :: NbPartTot
	integer, dimension(1:2), intent(in) :: NbPart
	integer, dimension(1:2), intent(inout) :: NbAcceptedPD,NbCallPD
	real(8) :: Random,acceptation,Bolzmann
	real(8), dimension(0:2) :: Displacement, PosPeriodicParts_ij
	real(8), dimension(1:2,0:NbPartTot-1,0:NbPartTot-1),intent(inout) :: PairsPot
	real(8), dimension(0:NbPartTot-1,0:NbPartTot-1) :: TempPairsPot
	real(8), intent(in) :: epsi,sigma,beta
	real(8), dimension(1:2), intent(in) :: AmpPD,BoxL,Rcut
	real(8), dimension(1:2), intent(inout) :: Potential
	real(8), dimension(1:2,0:NbPartTot-1,0:2), intent(inout) :: PosParts
	real(8) :: r_ij

	call RANDOM_NUMBER(Random)
	TempPart = FLOOR(Random*NbPartTot)
	call RANDOM_NUMBER(Displacement)

	if (TempPart.LT.NbPart(1)) then !!revoir la distri des proba entre 1 et 2
		b = 1
	else
		b = 2
		TempPart = TempPart - NbPart(1)
    end if

    NbCallPD(b) = NbCallPD(b) + 1

	TempPairsPot = PairsPot(b,:,:)
	Displacement = AmpPD(b)*(Displacement-0.5)*BoxL(b)

	do i=0,NbPart(b)-1
		if (i.NE.TempPart) then
			PosPeriodicParts_ij = PosParts(b,i,:)-(PosParts(b,TempPart,:)+displacement) &
				&-BoxL(b)*ANINT((PosParts(b,i,:)-(PosParts(b,TempPart,:)+displacement))/BoxL(b))
			r_ij = SQRT(SUM((PosPeriodicParts_ij)**2))
			if (r_ij.LT.Rcut(b)) then !Potential truncation?
				TempPairsPot(i,TempPart) = 4*epsi*((sigma/r_ij)**12-(sigma/r_ij)**6)
				TempPairsPot(TempPart,i) = TempPairsPot(i,TempPart)
			else
				TempPairsPot(i,TempPart) = 0
				TempPairsPot(TempPart,i) = TempPairsPot(i,TempPart)
			end if
		end if
	end do
	TempPartPot = SUM(TempPairsPot(TempPart,:))
	PartPot = SUM(PairsPot(b,TempPart,:))


	call RANDOM_NUMBER(acceptation)
	Boltzmann = EXP(-beta*(TempPartPot-PartPot))
	if (acceptation.LT.Boltzmann) then
		Potential(b) = Potential(b) + TempPartPot - PartPot
		NbAcceptedPD(b)=NbAcceptedPD(b)+1
		PairsPot(b,:,:) = TempPairsPot
		PosParts(b,TempPart,:) = PosParts(b,TempPart,:)+Displacement-BoxL(b)*ANINT((PosParts(b,TempPart,:)+Displacement)/BoxL(b))
	end if
END SUBROUTINE TRIAL_PARTICULE_DISPLACEMENT

SUBROUTINE TRIAL_VOLUME_CHANGE(NbPartTot,VolumeTot,NbPart,PosParts,PairsPot,BoxL,Potential,Rcut,&
		&epsi,sigma,AmpVC,beta,NbAcceptedVC)
	integer :: i=0
	integer, intent(in) :: NbPartTot
	integer, dimension(1:2), intent(in) :: NbPart
	integer, intent(inout) :: NbAcceptedVC
	real(8) :: acceptation, Boltzmann
	real(8), dimension(1:2) :: TempPotential, TempBoxL, RatioD, VolChange, TempRcut, Vol
	real(8), dimension(1:2,0:NbPartTot-1,0:NbPartTot-1),intent(out) :: PairsPot
	real(8), dimension(1:2,0:NbPartTot-1,0:NbPartTot-1) :: TempPairsPot
	real(8), intent(in) :: epsi, sigma,AmpVC,beta,VolumeTot
	real(8), dimension(1:2), intent(inout) :: Potential,BoxL,Rcut
	real(8), dimension(1:2, 0:NbPartTot-1,0:2), intent(inout) :: PosParts
	real(8), dimension(1:2, 0:NbPartTot-1,0:2) :: TempPosParts

	call RANDOM_NUMBER(VolChange(1))
	Vol=BoxL**3
	VolChange(1) = LOG(Vol(1)/Vol(2))+AmpVC*(VolChange(1)-0.5)
	VolChange(1) = VolumeTot*EXP(VolChange(1))/(1+EXP(VolChange(1)))
	VolChange(2) = VolumeTot-VolChange(1)
	TempBoxL = VolChange**(1/3.0)
	RatioD = TempBoxL/BoxL
	TempPosParts(1,:,:) = RatioD(1)*PosParts(1,:,:)
	TempPosParts(2,:,:) = RatioD(2)*PosParts(2,:,:)
	TempPairsPot = 0
	TempPotential = 0
	TempRcut = TempBoxL/2.0

	call POTENTIAL_INIT(NbPartTot,NbPart(1),TempPosParts(1,:,:),TempPairsPot(1,:,:),&
        &TempBoxL(1),TempPotential(1),TempRcut(1),epsi,sigma)
	call POTENTIAL_INIT(NbPartTot,NbPart(2),TempPosParts(2,:,:),TempPairsPot(2,:,:),&
        &TempBoxL(2),TempPotential(2),TempRcut(2),epsi,sigma)

	call RANDOM_NUMBER(acceptation)
	Boltzmann = (VolChange(1)/Vol(1))**(NbPart(1)+1)*(VolChange(2)/Vol(2))**(NbPart(2)+1)&
		&*EXP(-beta*(SUM(TempPotential)-SUM(Potential)))

	if (acceptation.LT.Boltzmann) then
		Potential = TempPotential
		NbAcceptedVC=NbAcceptedVC+1
		PairsPot = TempPairsPot
		PosParts = TempPosParts
		BoxL = TempBoxL
		Rcut = TempRcut
	end if
END SUBROUTINE TRIAL_VOLUME_CHANGE

SUBROUTINE TRIAL_PARTICULE_EXCHANGE(NbPartTot,NbPart,PosParts,PairsPot,BoxL,Potential,Rcut,&
		&epsi,sigma,Mu,beta,NbCallPE,NbAcceptedPE)
	integer :: TempPart, in, out
	integer, intent(in) :: NbPartTot
	integer, intent(inout) :: NbAcceptedPE
	integer, dimension(1:2), intent(inout) :: NbPart,NbCallPE
	real(8) :: Random, acceptation, Boltzmann,r_ij
	real(8), dimension(1:2) :: TempPotential, TempBoxL
	real(8), dimension(0:2) :: Displacement,PosPeriodicParts_ij
	real(8), dimension(1:2,0:NbPartTot-1,0:NbPartTot-1),intent(inout) :: PairsPot
	real(8), dimension(0:NbPartTot) :: PartInPot
	real(8), intent(in) :: epsi,sigma,beta
	real(8), dimension(1:2), intent(inout) :: Potential, Mu
	real(8), dimension(1:2), intent(in) :: BoxL,Rcut
	real(8), dimension(1:2, 0:NbPartTot-1,0:2), intent(inout) :: PosParts

	call RANDOM_NUMBER(Random)
	in = INT(Random*2)+1
	out = -1*INT(Random*2)+2
	call RANDOM_NUMBER(Random)
	TempPart = INT(Random*NbPart(out))
	call RANDOM_NUMBER(Displacement)
	Displacement = (Displacement-0.5)*BoxL(in)
	PartInPot = 0
	NbCallPE(in) = NbCallPE(in)+1

	do i=0,NbPart(in)-1
		PosPeriodicParts_ij = PosParts(in,i,:)-Displacement&
			&-BoxL(in)*ANINT((PosParts(in,i,:)-Displacement)/BoxL(in))
		r_ij = SQRT(SUM((PosPeriodicParts_ij)**2))
		if (r_ij.LT.Rcut(in)) then !Potential truncation?
			PartInPot(i) = 4*epsi*((sigma/r_ij)**12-(sigma/r_ij)**6)
		else
			PartInPot(i) = 0
		end if
	end do

	Mu(in) = Mu(in) + (BoxL(in)**3/(NbPart(in)+1))*EXP(-beta*SUM(PartInPot))!!!!!!!!

	TempPotential(in) = Potential(in) + SUM(PartInPot)
	TempPotential(out) = Potential(out) - SUM(PairsPot(out,TempPart,:))

	call RANDOM_NUMBER(acceptation)
	Boltzmann = ((NbPart(out)*BoxL(in)**3)/((NbPart(in)+1)*BoxL(out)**3))&
		&*EXP(-beta*(SUM(TempPotential)-SUM(Potential)))
	if (acceptation.LT.Boltzmann) then
		Potential = TempPotential

		PosParts(in,NbPart(in),:) = Displacement
		PosParts(out,TempPart,:) = PosParts(out,NbPart(out)-1,:)
		PosParts(out,Nbpart(out)-1,:) = 0

		PairsPot(out,TempPart,:) = PairsPot(out,NbPart(out)-1,:)
		PairsPot(out,NbPart(out)-1,:) = 0
		PairsPot(out,:,NbPart(out)-1) = 0
		PairsPot(out,TempPart,TempPart) = 0
		PairsPot(out,:,TempPart) = PairsPot(out,TempPart,:)
		PairsPot(in,NbPart(in),0:NbPart(in)-1) = PartInPot(0:NbPart(in)-1)
		PairsPot(in,0:NbPart(in)-1,NbPart(in)) = PartInPot(0:NbPart(in)-1)

		NbPart(in) = NbPart(in)+1
		NbPart(out) = NbPart(out)-1
		NbAcceptedPE = NbAcceptedPE+1
	end if
END SUBROUTINE TRIAL_PARTICULE_EXCHANGE

SUBROUTINE RADIAL_DISTRIBUTION(NbPartTot,NbPart,PosParts,BoxL,RadialDistribution,RadialIndex,Nb12Loop,NbPair)

	integer, intent(in) :: NbPart,Nb12Loop,NbPair
	real(8),intent(in) :: BoxL
	integer :: i=0,j=0
	integer, intent(inout) :: RadialIndex
	real(8), dimension(0:NbPartTot-1,0:2),intent(in) :: PosParts
	real(8), dimension(0:Nb12Loop*NbPair-1), intent(inout) :: RadialDistribution
	real(8), dimension(0:2) :: PosPeriodicParts_ij

	do i=0,NbPart-2
		do j=i+1,NbPart-1
			PosPeriodicParts_ij = PosParts(i,:)-PosParts(j,:)-BoxL*ANINT((PosParts(i,:)-PosParts(j,:))/BoxL)
			RadialDistribution(RadialIndex) = SQRT(SUM((PosPeriodicParts_ij)**2)) !normalization volume
			RadialIndex=RadialIndex+1
		end do
	end do
END SUBROUTINE RADIAL_DISTRIBUTION

SUBROUTINE AMP_UPDATES(Amp,NbAccepted,NbCall)
	integer, intent(inout) :: NbAccepted, NbCall
	real(8), intent(inout) :: Amp
	if ((NbAccepted/REAL(NbCall)).LT.0.4) then !working for VC?
		Amp = Amp*0.9
	else if	((NbAccepted/REAL(NbCall)).GT.0.6) then
		Amp = Amp*1.1
	end if
	NbAccepted = 0
	NbCall = 0
END SUBROUTINE AMP_UPDATES

END MODULE SUBROUTINES

!----------MAIN----------!

PROGRAM LIQUID_POT

	USE SUBROUTINES
	IMPLICIT NONE
	integer :: NbPartTot,Nb1Loop,Nb2Loop,NbPair,Nb12Loop,Nb3Loop
	integer :: i,j,k,l=0,NbAcceptedVC=0,NbCallVC=0,NbAcceptedPE=0
	integer, dimension(1:2) :: NbCallPE=0,RadialIndex=0,NbPart,NbAcceptedPD=0,NbCallPD=0,TempRadialIndex=0
	integer, dimension(:,:), allocatable :: ListNbPart
	real(8), parameter :: epsi=1, sigma=1
	real(8) :: AmpVC=0.2,beta,Temp,Random,VolumeTot
	real(8), dimension(:,:,:), allocatable :: PairsPot, PosParts
	real(8), dimension(:,:), allocatable :: RadialDistribution, ListPotential, ListBoxL, ListMu, ListPressure
	real(8), dimension(1:2) :: Potential, BoxL, MeanBoxL, MeanVolume, MeanEnthalpy, MeanNbPart, MeanMu, Rcut,&
		&AmpPD=0.1, MeanPotential, Cv, Mu, Pressure, MeanDens
	CHARACTER(len=32) :: OutputFilePath

	Nb1Loop=100
	Nb2Loop=100

	call READ_INPUTFILE(NbPartTot,NbPart,BoxL,PosParts,Temp,OutputFilePath,Nb1Loop,Nb2Loop)


	Rcut = BoxL/2
	VolumeTot = BoxL(2)**3+BoxL(1)**3

	Nb3Loop=2*NbPartTot
	NbPair=((NbPartTot-1)*NbPartTot)/2
	Nb12Loop=Nb1Loop*Nb2Loop

	allocate(PairsPot(1:2,0:NbPartTot-1,0:NbPartTot-1))
	allocate(RadialDistribution(1:2,0:NbPair*Nb12Loop-1))
	allocate(ListPotential(1:2,0:Nb12Loop-1))
	allocate(ListBoxL(1:2,0:Nb12Loop-1))
	allocate(ListNbPart(1:2,0:Nb12Loop-1))
	allocate(ListMu(1:2,0:Nb1Loop-1))
	allocate(ListPressure(1:2,0:Nb12Loop-1))


	beta = 1/Temp
	RadialIndex=0
	PairsPot=0
	RadialDistribution=0


	call POTENTIAL_INIT(NbPartTot,NbPart(1),PosParts(1,:,:),PairsPot(1,:,:),BoxL(1),Potential(1),Rcut(1),epsi,sigma)
	call POTENTIAL_INIT(NbPartTot,NbPart(2),PosParts(2,:,:),PairsPot(2,:,:),BoxL(2),Potential(2),Rcut(2),epsi,sigma)
	call RANDOM_INIT()

	do i=0,Nb1Loop-1 !1Loop Updates of the displacement amplitude parameter
          Mu = 0
		do j=0,Nb2Loop-1 !2Loop Updates of the physical quantities
			do k=0,Nb3Loop-1 !3Loop Uncorrelation of physical observables
				call RANDOM_NUMBER(Random)
				if (Random .LT. 0.5) then !ajouter le potentiel chimique
						call TRIAL_PARTICULE_EXCHANGE(NbPartTot,NbPart,PosParts,PairsPot,BoxL,Potential,Rcut,&
							&epsi,sigma,Mu,beta,NbCallPE,NbAcceptedPE)
				else if (Random .LT. 0.51) then
						call TRIAL_VOLUME_CHANGE(NbPartTot,VolumeTot,NbPart,PosParts,PairsPot,BoxL,Potential,Rcut,&
								&epsi,sigma,AmpVC,beta,NbAcceptedVC)
						NbCallVC = NbCallVC+1
				else
						call TRIAL_PARTICULE_DISPLACEMENT(NbPartTot,NbPart,PosParts,PairsPot,BoxL,Potential,Rcut,&
							&epsi,sigma,AmpPD,beta,NbAcceptedPD,NbCallPD)
				end if
			end do
			TempRadialIndex = RadialIndex
			call RADIAL_DISTRIBUTION(NbPartTot,NbPart(1),PosParts(1,:,:),BoxL(1),RadialDistribution(1,:),RadialIndex(1),Nb12Loop,NbPair)
			call RADIAL_DISTRIBUTION(NbPartTot,NbPart(2),PosParts(2,:,:),BoxL(2),RadialDistribution(2,:),RadialIndex(2),Nb12Loop,NbPair)
			ListPotential(:,i*Nb2Loop+j) = Potential
			ListBoxL(:,i*Nb2Loop+j) = BoxL
			ListNbPart(:,i*Nb2Loop+j) = NbPart
			ListPressure(1,i*Nb2Loop+j) = (1/BoxL(1)**3)*SUM(24*epsi*(2*&
		          &(sigma/RadialDistribution(1,TempRadialIndex(1):RadialIndex(1)-1))**12&
		          &-(sigma/RadialDistribution(1,TempRadialIndex(1):RadialIndex(1)-1))**6))
			ListPressure(2,i*Nb2Loop+j) = (1/BoxL(2)**3)*SUM(24*epsi*(2*&
		          &(sigma/RadialDistribution(2,TempRadialIndex(2):RadialIndex(2)-1))**12&
		          &-(sigma/RadialDistribution(2,TempRadialIndex(2):RadialIndex(2)-1))**6))

		end do
		ListMu(:,i) = -Temp*LOG(Mu/NbCallPE) !add thermal Brooglie wavelength
		print*,'PD',NbCallPD,NbAcceptedPD,AmpPD
		print*,'VC',NbCallVC,NbAcceptedVC,AmpVC
		print*,'PE',NbCallPE,NbAcceptedPE,Mu
		call AMP_UPDATES(AmpPD(1),NbAcceptedPD(1),NbCallPD(1))
		call AMP_UPDATES(AmpPD(2),NbAcceptedPD(2),NbCallPD(2))
		call AMP_UPDATES(AmpVC,NbAcceptedVC,NbCallVC)
		NbCallPE = 0
		NbAcceptedPE = 0
		print*,'Loading...',(i*100)/Nb1Loop,'%'
	end do


	MeanPotential = SUM(ListPotential(:,Nb12Loop/2-1:),DIM=2)/(Nb12Loop/2)
	MeanBoxL = SUM(ListBoxL(:,Nb12Loop/2-1:),DIM=2)/(Nb12Loop/2)
	MeanVolume = SUM(ListBoxL(:,Nb12Loop/2-1:)**3,DIM=2)/(Nb12Loop/2)
	MeanNbPart = SUM(ListNbPart(:,Nb12Loop/2-1:),DIM=2)/Real(Nb12Loop/2)
	MeanMu = SUM(ListMu(:,Nb1Loop/2-1:),DIM=2)/(Nb1Loop/2)
	MeanDens = SUM(ListNbPart(:,Nb12Loop/2-1:)/ListBoxL(:,Nb12Loop/2-1:)**3,DIM=2)/(Nb12Loop/2)
	Rcut = MeanBoxL/2
	!Cv = beta**2*(SUM(ListPotential(Nb12Loop/2-1:)**2)/(Nb12Loop/2)-MeanPotential**2) !not sure
  Pressure = MeanDens/beta+(1/6.0)*SUM(ListPressure(:,Nb12Loop/2-1:),DIM=2)/(Nb12Loop/2)&
					&+4*epsi*MeanDens**2*(((2*sigma**12)/(11*Rcut**11))-(sigma**6/(5*Rcut**5)))
	MeanEnthalpy = MeanPotential + MeanVolume*Pressure
	print*, MeanDens, Pressure, (1/6.0)*SUM(ListPressure(:,Nb12Loop/2-1:),DIM=2)/(Nb12Loop/2)

	call WRITE_OUTPUTFILE(NbPartTot,NbPart,MeanBoxL,Nb1Loop,Nb12Loop,NbPair,PosParts,RadialDistribution,MeanPotential&
		&,Pressure,MeanVolume,MeanEnthalpy,ListBoxL,ListNbPart,VolumeTot,ListPotential,ListMu,OutputFilePath,MeanNbPart,MeanMu)

	deallocate(PosParts)
	deallocate(PairsPot)
	deallocate(RadialDistribution)
	deallocate(ListPotential)
	deallocate(ListNbPart)
	deallocate(ListMu)
	deallocate(ListBoxL)
END PROGRAM LIQUID_POT
