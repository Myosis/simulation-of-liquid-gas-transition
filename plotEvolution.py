import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

######

# base = 0.1**(-0.1*np.arange(-5,36))
# MeanPotential = np.genfromtxt('MeanPotential.txt')
# fig,ax = plt.subplots()
# ax.plot(MeanPotential)

######

#base = np.arange(0,5.43,0.01)
#density = np.genfromtxt('E_T_L6_5.txt')
#fig,ax = plt.subplots()
#ax.plot(density,'o',label=r'$\rho$(P)')
#ax.set_ylabel('Density',fontsize=14)
#ax.set_xlabel('Pressure (reduced unit)',fontsize=14)
#plt.legend()


######

# base = np.arange(6.3,6.50,0.01)
# Pressure = np.genfromtxt('Pressure.txt')
# fig,ax = plt.subplots()
# ax.plot(base,Pressure,'o',label='P(V)')

######

# base = np.arange(0.1,2.05,0.1)
# BoxL = np.genfromtxt('BoxL.txt')
# #fig,ax = plt.subplots()
# ax.plot(BoxL,base,'o',label='V(P)')
# ax.set_xlabel('Box length',fontsize=14)
# ax.set_ylabel('Pressure (reduced unit)',fontsize=14)
# plt.legend()

######

# Cv = np.genfromtxt('Cv.txt')
# fig,ax = plt.subplots()
# ax.plot(Cv)

######
path = 'T11D0323/'

Potential = np.fromfile(path+'PotentialEvol.dat',dtype=np.float32)

fig,ax = plt.subplots()
ax.plot(Potential[0::2])
ax.plot(Potential[1::2])

######

#PosPartsEnd = np.fromfile('PosPartsEnd.dat',dtype=np.float32)
#PosPartsEnd = PosPartsEnd.reshape((3,-1))

#fig = plt.figure()
#ax = fig.add_subplot(111, projection='3d')
#ax.scatter(PosPartsEnd[0,:],PosPartsEnd[1,:],PosPartsEnd[2,:])

######

#RadialDistribution = np.fromfile('RadialDistribution.dat',dtype=np.float32)

#NbBins = 100
#bins = np.linspace(RadialDistribution.min(),RadialDistribution.max(),NbBins)
#Volume = (4/3)*np.pi*(bins[1:]**3 - bins[:-1]**3)

#fig = plt.figure()
#counts, bins = np.histogram(RadialDistribution,bins=bins)
#counts=counts/Volume
#n, bins, patches = plt.hist(bins[:-1], bins, weights=counts, normed=True)
#plt.grid(True)

######

#MeanPotentials = np.fromfile('MeanPotentials.dat',dtype=np.float32)
#fig,ax = plt.subplots()
#ax.plot(MeanPotentials)

######

plt.show()
