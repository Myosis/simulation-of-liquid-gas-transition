SUBROUTINE WRITE_INPUTFILE(NbPart,BoxL,PosParts1,PosParts2)
	integer, dimension(1:2), intent(in) :: NbPart
	real, dimension(1:2), intent(in) :: BoxL
	integer :: i
	real, dimension(0:NbPart(1)-1,0:2), intent(in) :: PosParts1
	real, dimension(0:NbPart(2)-1,0:2), intent(in) :: PosParts2
	CHARACTER(len=3) :: WhichParts = 'Ar '

	open(12, file='FCcubicGibbs.if',access='sequential', form='formatted', status="unknown", action="write")
	write(12,*) SUM(NbPart),NbPart(1),NbPart(2)
	write(12,*) BoxL(1),BoxL(2)
	do i=0,NbPart(1)-1
		write(12,113) WhichParts,PosParts1(i,:)
		113 format (A3,3ES15.7)
	end do
	do i=0,NbPart(2)-1
		write(12,114) WhichParts,PosParts2(i,:)
		114 format (A3,3ES15.7)
	end do
	close(12)
END SUBROUTINE WRITE_INPUTFILE

PROGRAM GEN_INPUTFILE

	logical, parameter :: cubic=.True. !Faces centered cubic NbPart=4*x**3
	integer, dimension(1:2), parameter :: NbPart=(/256,256/)
	real, dimension(1:2), parameter :: BoxL=(/7.5,7.5/)
	integer :: i=0,k,x,y,z
	integer, dimension(0:7) :: values
	integer, dimension(:), allocatable :: seed
	real, dimension(0:NbPart(1)-1,0:2) :: PosParts1
	real, dimension(0:NbPart(2)-1,0:2) :: PosParts2


	call date_and_time(values=values)
	call RANDOM_SEED(size=k)
	allocate(seed(1:k))
	seed(:) = values(7)
	call RANDOM_SEED(put=seed)

    if (cubic) then
		NbPart1D = NINT((NbPart(1)/4)**(1.0/3.0))
		do x=0,NbPart1D-1
			do y=0,NbPart1D-1
				do z=0,NbPart1D-1
					PosParts1(i,0)=BoxL(1)*(-0.5+x/REAL(NbPart1D))
					PosParts1(i,1)=BoxL(1)*(-0.5+y/REAL(NbPart1D))
					PosParts1(i,2)=BoxL(1)*(-0.5+z/REAL(NbPart1D))
					i=i+1
				end do
			end do
		end do
		PosParts1(NbPart(1)/4:NbPart(1)/2-1,0) = PosParts1(:NbPart(1)/4-1,0) + 0.5*BoxL(1)/NbPart1D
		PosParts1(NbPart(1)/4:NbPart(1)/2-1,1) = PosParts1(:NbPart(1)/4-1,1) + 0.5*BoxL(1)/NbPart1D
		PosParts1(NbPart(1)/4:NbPart(1)/2-1,2) = PosParts1(:NbPart(1)/4-1,2)
		PosParts1(NbPart(1)/2:3*NbPart(1)/4-1,0) = PosParts1(:NbPart(1)/4-1,0)
		PosParts1(NbPart(1)/2:3*NbPart(1)/4-1,1) = PosParts1(:NbPart(1)/4-1,1) + 0.5*BoxL(1)/NbPart1D
		PosParts1(NbPart(1)/2:3*NbPart(1)/4-1,2) = PosParts1(:NbPart(1)/4-1,2) + 0.5*BoxL(1)/NbPart1D
		PosParts1(3*NbPart(1)/4:NbPart(1)-1,0) = PosParts1(:NbPart(1)/4-1,0) + 0.5*BoxL(1)/NbPart1D
		PosParts1(3*NbPart(1)/4:NbPart(1)-1,1) = PosParts1(:NbPart(1)/4-1,1)
		PosParts1(3*NbPart(1)/4:NbPart(1)-1,2) = PosParts1(:NbPart(1)/4-1,2) + 0.5*BoxL(1)/NbPart1D
		i=0
		NbPart1D = NINT((NbPart(2)/4)**(1.0/3.0))
		do x=0,NbPart1D-1
			do y=0,NbPart1D-1
				do z=0,NbPart1D-1
					PosParts2(i,0)=BoxL(2)*(-0.5+x/REAL(NbPart1D))
					PosParts2(i,1)=BoxL(2)*(-0.5+y/REAL(NbPart1D))
					PosParts2(i,2)=BoxL(2)*(-0.5+z/REAL(NbPart1D))
					i=i+1
				end do
			end do
		end do
		PosParts2(NbPart(2)/4:NbPart(2)/2-1,0) = PosParts2(:NbPart(2)/4-1,0) + 0.5*BoxL(2)/NbPart1D
		PosParts2(NbPart(2)/4:NbPart(2)/2-1,1) = PosParts2(:NbPart(2)/4-1,1) + 0.5*BoxL(2)/NbPart1D
		PosParts2(NbPart(2)/4:NbPart(2)/2-1,2) = PosParts2(:NbPart(2)/4-1,2)
		PosParts2(NbPart(2)/2:3*NbPart(2)/4-1,0) = PosParts2(:NbPart(2)/4-1,0)
		PosParts2(NbPart(2)/2:3*NbPart(2)/4-1,1) = PosParts2(:NbPart(2)/4-1,1) + 0.5*BoxL(2)/NbPart1D
		PosParts2(NbPart(2)/2:3*NbPart(2)/4-1,2) = PosParts2(:NbPart(2)/4-1,2) + 0.5*BoxL(2)/NbPart1D
		PosParts2(3*NbPart(2)/4:NbPart(2)-1,0) = PosParts2(:NbPart(2)/4-1,0) + 0.5*BoxL(2)/NbPart1D
		PosParts2(3*NbPart(2)/4:NbPart(2)-1,1) = PosParts2(:NbPart(2)/4-1,1)
		PosParts2(3*NbPart(2)/4:NbPart(2)-1,2) = PosParts2(:NbPart(2)/4-1,2) + 0.5*BoxL(2)/NbPart1D
	else
        call RANDOM_NUMBER(PosParts1)
        PosParts1 = (PosParts1-0.5)*BoxL(1)
        call RANDOM_NUMBER(PosParts2)
        PosParts2 = (PosParts2-0.5)*BoxL(2)
    end if

	call WRITE_INPUTFILE(NbPart,BoxL,PosParts1,PosParts2)

END PROGRAM GEN_INPUTFILE
