SUBROUTINE WRITE_INPUTFILE(NbPart,BoxL,PosParts1,PosParts2)
	integer, dimension(1:2), intent(in) :: NbPart
	real(8), dimension(1:2), intent(in) :: BoxL
	integer :: i
	real(8), dimension(0:NbPart(1)-1,0:2), intent(in) :: PosParts1
	real(8), dimension(0:NbPart(2)-1,0:2), intent(in) :: PosParts2
	CHARACTER(len=3) :: WhichParts = 'Ar '

	open(12, file='cubicHg.if',access='sequential', form='formatted', status="unknown", action="write")
	write(12,*) SUM(NbPart),NbPart(1),NbPart(2)
	write(12,*) BoxL(1),BoxL(2)
	do i=0,NbPart(1)-1
		write(12,113) WhichParts,PosParts1(i,:)
		113 format (A3,3ES15.7)
	end do
	do i=0,NbPart(2)-1
		write(12,114) WhichParts,PosParts2(i,:)
		114 format (A3,3ES15.7)
	end do
	close(12)
END SUBROUTINE WRITE_INPUTFILE

SUBROUTINE CUBIC_F(NbPart,BoxL,PosParts)
	integer, intent(in) :: NbPart
	integer :: NbPart1D
	real(8), intent(in) :: BoxL
	integer :: i,x,y,z
	real(8), dimension(0:NbPart-1,0:2), intent(out) :: PosParts
	i=0
	NbPart1D = CEILING(NbPart**(1.0/3.0))
	do x=0,NbPart1D-1
		do y=0,NbPart1D-1
			do z=0,NbPart1D-1
				if(i.LT.NbPart)then
					PosParts(i,0)=BoxL*(-0.5+x/REAL(NbPart1D))
					PosParts(i,1)=BoxL*(-0.5+y/REAL(NbPart1D))
					PosParts(i,2)=BoxL*(-0.5+z/REAL(NbPart1D))
					i=i+1
				end if
			end do
		end do
	end do
END SUBROUTINE CUBIC_F

PROGRAM GEN_INPUTFILE
	IMPLICIT NONE
	integer, dimension(1:2), parameter :: NbPart=(/216,216/)
	real(8), dimension(1:2), parameter :: BoxL=(/50,50/)
	real(8), dimension(0:NbPart(1)-1,0:2) :: PosParts1
	real(8), dimension(0:NbPart(2)-1,0:2) :: PosParts2

	call CUBIC_F(NbPart(1),BoxL(1),PosParts1)
	call CUBIC_F(NbPart(2),BoxL(2),PosParts2)
	call WRITE_INPUTFILE(NbPart,BoxL,PosParts1,PosParts2)

END PROGRAM GEN_INPUTFILE
