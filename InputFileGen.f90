SUBROUTINE WRITE_INPUTFILE(NbPart,BoxL,PosParts)
	integer, intent(in) :: NbPart
	real,intent(in) :: BoxL
	integer :: i
	real, dimension(0:NbPart-1,0:2),intent(in) :: PosParts
	CHARACTER(len=3) :: WhichParts = 'Ar '
    		
	open(12, file='FCcubic.if',access='sequential', form='formatted', status="unknown", action="write")
	write(12,*) NbPart
	write(12,*) BoxL
	do i=0,NbPart-1
		write(12,113) WhichParts,PosParts(i,:)
		113 format (A3,3ES15.7)
	end do
	close(12)
END SUBROUTINE WRITE_INPUTFILE

PROGRAM GEN_INPUTFILE
	
	logical, parameter :: cubic=.True. !Faces centered cubic NbPart=4*x**3
	integer, parameter :: NbPart=4*4**3
	real,parameter :: BoxL=8
	integer :: i=0,k,x,y,z
	integer, dimension(0:7) :: values
	integer, dimension(:), allocatable :: seed
	real, dimension(0:NbPart-1,0:2) :: PosParts

	call date_and_time(values=values)
	call RANDOM_SEED(size=k)
	allocate(seed(1:k))
	seed(:) = values(7)
	call RANDOM_SEED(put=seed)

	if (cubic) then
		NbPart1D = NINT((NbPart/4)**(1.0/3.0))
		do x=0,NbPart1D-1
			do y=0,NbPart1D-1
				do z=0,NbPart1D-1
					PosParts(i,0)=BoxL*(-0.5+x/REAL(NbPart1D))
					PosParts(i,1)=BoxL*(-0.5+y/REAL(NbPart1D))
					PosParts(i,2)=BoxL*(-0.5+z/REAL(NbPart1D))
					i=i+1
				end do
			end do
		end do
		PosParts(NbPart/4:NbPart/2-1,0) = PosParts(:NbPart/4-1,0) + 0.5*BoxL/NbPart1D
		PosParts(NbPart/4:NbPart/2-1,1) = PosParts(:NbPart/4-1,1) + 0.5*BoxL/NbPart1D
		PosParts(NbPart/4:NbPart/2-1,2) = PosParts(:NbPart/4-1,2)
		PosParts(NbPart/2:3*NbPart/4-1,0) = PosParts(:NbPart/4-1,0)
		PosParts(NbPart/2:3*NbPart/4-1,1) = PosParts(:NbPart/4-1,1) + 0.5*BoxL/NbPart1D
		PosParts(NbPart/2:3*NbPart/4-1,2) = PosParts(:NbPart/4-1,2) + 0.5*BoxL/NbPart1D
		PosParts(3*NbPart/4:NbPart-1,0) = PosParts(:NbPart/4-1,0) + 0.5*BoxL/NbPart1D
		PosParts(3*NbPart/4:NbPart-1,1) = PosParts(:NbPart/4-1,1)
		PosParts(3*NbPart/4:NbPart-1,2) = PosParts(:NbPart/4-1,2) + 0.5*BoxL/NbPart1D
	else
		call RANDOM_NUMBER(PosParts)
		PosParts = (PosParts-0.5)*BoxL
	end if

	call WRITE_INPUTFILE(NbPart,BoxL,PosParts)

END PROGRAM GEN_INPUTFILE
