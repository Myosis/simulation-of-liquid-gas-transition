!----------SUBROUTINES----------!

MODULE SUBROUTINES
	contains
SUBROUTINE LJ_POTENTIAL(r,Pot)
	 real, intent(in) :: r
	 real, intent(out) :: Pot
   Pot = 4*((1.0/r)**12-(1.0/r)**6)
end SUBROUTINE LJ_POTENTIAL

SUBROUTINE READ_INPUTFILE(NbPart,BoxL,PosParts,Temp,Nb1Loop,Nb2Loop)
	integer, intent(out) :: NbPart
	integer, intent(in) :: Nb1Loop,Nb2Loop
	integer :: i,j
  real :: Density, Ratio
	real, intent(out) :: BoxL
	real, intent(out) :: Temp
	real, dimension(:,:), allocatable, intent(out) :: PosParts
	CHARACTER(len=32) :: InputFilePath,TempString,DensityString

  CALL GET_COMMAND_ARGUMENT(1, InputFilePath)
	CALL GET_COMMAND_ARGUMENT(2, TempString)
	CALL GET_COMMAND_ARGUMENT(3, DensityString)


	IF (LEN_TRIM(InputFilePath) == 0) then
		STOP 'No input file as argument'
	else
		open(12, file=TRIM(InputFilePath),access='sequential', form='formatted', status="old", action="read")
		read(12,*) NbPart
		read(12,*) BoxL
		allocate(PosParts(0:NbPart-1,0:2))
		do i=0,NbPart-1
			read(12,113) WhichParts,PosParts(i,:)
			113 format (A3,3ES15.7) !implement argument
		end do
		close(12)
	end if

	IF (LEN_TRIM(TempString) == 0) then
		STOP 'No Temperature as argument'
	else
		TempString = TRIM(TempString)
		read(TempString,*) Temp
	end if

  IF (LEN_TRIM(DensityString) == 0) then
        STOP 'No Density as argument'
  else
    DensityString = TRIM(DensityString)
    read(DensityString,*) Density
    Ratio = ((NbPart/BoxL**3)/Density)**(1/3.0)
    BoxL = BoxL*Ratio
    PosParts(:,:) = Ratio*PosParts(:,:)
  end if

	open(12, file="simulation.txt",access='sequential', form='formatted', status="unknown", action="write")
	write(12,*) 'Number of particules',NbPart
	write(12,*) 'Volumes',BoxL**3
	write(12,*) 'Temperature',Temp
	write(12,*) 'Number of loops',Nb1Loop,Nb2Loop
	close(12)
END SUBROUTINE READ_INPUTFILE

SUBROUTINE WRITE_OUTPUTFILE(NbPart,BoxL,Nb1Loop,Nb12Loop,NbPair,PosParts,RadialDistribution,MeanPotential&
		&,Pressure,ListPotential)

	integer, intent(in) :: Nb12Loop,NbPair
	integer, intent(in) :: NbPart
	integer :: i
	integer, dimension(0:Nb12Loop-1) ::ListNbPart
	real, intent(in) :: MeanPotential,BoxL,Pressure
	real, dimension(0:NbPart-1,0:2),intent(in) :: PosParts
	real, dimension(0:Nb12Loop*NbPair-1), intent(in) :: RadialDistribution
	real, dimension(0:Nb12Loop-1) :: ListPotential
	CHARACTER(len=3) :: WhichParts = 'Ar '

	open(12, file="VolumeEvol.dat", status="unknown", access="stream", action="write")
	write(12) ListBoxL**3
	close(12)

	open(12, file="PotentialEvol.dat", status="unknown", access="stream", action="write")
	write(12) ListPotential
	close(12)

	open(12, file="simulation.txt",access='append', form='formatted', status="old", action="write")
	write(12,*) 'Potential         ',MeanPotential
	write(12,*) 'Pressure          ',Pressure
	close(12)

	open(12, file="gibbs.of",access='sequential', form='formatted', status="unknown", action="write")
	write(12,*) NbPart
	write(12,*) BoxL
	do i=0,NbPart-1
		write(12,113) WhichParts,PosParts(i,:)
		113 format (A3,3ES15.7)
	end do
	close(12)
END SUBROUTINE WRITE_OUTPUTFILE

SUBROUTINE POTENTIAL_INIT(NbPart,PosParts,PairsPot,BoxL,Potential,Rcut)

	real, intent(in) :: BoxL, Rcut
	real, intent(out) :: Potential
	integer, intent(in) :: NbPart
	integer :: i=0,j=0
	real, dimension(0:NbPart-1,0:2),intent(in) :: PosParts
	real, dimension(0:2) :: PosPeriodicParts_ij
	real, dimension(0:NbPart-1,0:NbPart-1),intent(out) :: PairsPot
	real :: r_ij
	PairsPot = 0
	Potential = 0

	do i=0,NbPart-2
		do j=i+1,NbPart-1
			PosPeriodicParts_ij = PosParts(i,:)-PosParts(j,:)-BoxL*ANINT((PosParts(i,:)-PosParts(j,:))/BoxL)
			r_ij = SQRT(SUM((PosPeriodicParts_ij)**2))
			if (r_ij.LT.Rcut) then !Potential truncation?
				call LJ_POTENTIAL(r_ij,PairsPot(i,j))
				PairsPot(j,i) = PairsPot(i,j)
				Potential = Potential + PairsPot(i,j)
			else
				PairsPot(i,j) = 0
				PairsPot(j,i) = PairsPot(i,j)
			end if
		end do
	end do
END SUBROUTINE POTENTIAL_INIT

SUBROUTINE RANDOM_INIT()

	integer :: k
	integer, dimension(0:7) :: values
	integer, dimension(:), allocatable :: seed
	call date_and_time(values=values)
	call RANDOM_SEED(size=k)
	allocate(seed(1:k))
	seed(:) = values(7)
	call RANDOM_SEED(put=seed)
END SUBROUTINE RANDOM_INIT

SUBROUTINE TRIAL_PARTICULE_DISPLACEMENT(NbPart,PosParts,PairsPot,BoxL,Potential,Rcut,&
		&AmpPD,beta,NbAcceptedPD,NbCallPD)
	integer :: TempPart, i=0,b
	integer, intent(in) :: NbPart
	integer, intent(inout) :: NbAcceptedPD,NbCallPD
	real :: Random,acceptation,Bolzmann
	real, dimension(0:2) :: Displacement, PosPeriodicParts_ij
	real, dimension(0:NbPart-1,0:NbPart-1),intent(inout) :: PairsPot
	real, dimension(0:NbPart-1,0:NbPart-1) :: TempPairsPot
	real, intent(in) :: beta
	real, intent(in) :: AmpPD,BoxL,Rcut
	real, intent(inout) :: Potential
	real, dimension(0:NbPart-1,0:2), intent(inout) :: PosParts
	real :: r_ij

	call RANDOM_NUMBER(Random)
	TempPart = FLOOR(Random*NbPart)
	call RANDOM_NUMBER(Displacement)

    NbCallPD = NbCallPD + 1

	TempPairsPot = PairsPot(:,:)
	Displacement = AmpPD*(Displacement-0.5)*BoxL

	do i=0,NbPart-1
		if (i.NE.TempPart) then
			PosPeriodicParts_ij = PosParts(i,:)-(PosParts(TempPart,:)+displacement)&
				&-BoxL*ANINT((PosParts(i,:)-(PosParts(TempPart,:)+displacement))/BoxL)
			r_ij = SQRT(SUM((PosPeriodicParts_ij)**2))
			if (r_ij.LT.Rcut) then !Potential truncation?
				call LJ_POTENTIAL(r_ij,TempPairsPot(i,TempPart))
				TempPairsPot(TempPart,i) = TempPairsPot(i,TempPart)
			else
				TempPairsPot(i,TempPart) = 0
				TempPairsPot(TempPart,i) = TempPairsPot(i,TempPart)
			end if
		end if
	end do
	TempPartPot = SUM(TempPairsPot(TempPart,:))
	PartPot = SUM(PairsPot(TempPart,:))

	call RANDOM_NUMBER(acceptation)
	Boltzmann = EXP(-beta*(TempPartPot-PartPot))
	if (acceptation.LT.Boltzmann) then
		Potential = Potential + TempPartPot - PartPot
		NbAcceptedPD=NbAcceptedPD+1
		PairsPot(:,:) = TempPairsPot
		PosParts(TempPart,:) = PosParts(TempPart,:)+Displacement-BoxL*ANINT((PosParts(TempPart,:)+Displacement)/BoxL)
	end if
END SUBROUTINE TRIAL_PARTICULE_DISPLACEMENT

SUBROUTINE RADIAL_DISTRIBUTION(NbPart,PosParts,BoxL,RadialDistribution,Nb12Loop,NbPair)

	integer, intent(in) :: NbPart,Nb12Loop,NbPair
	real,intent(in) :: BoxL
	integer :: i=0,j=0
	integer :: RadialIndex
	real, dimension(0:NbPart-1,0:2),intent(in) :: PosParts
	real, dimension(0:Nb12Loop*NbPair-1), intent(inout) :: RadialDistribution
	real, dimension(0:2) :: PosPeriodicParts_ij
	real :: Pot
	RadialIndex=0

	do i=0,NbPart-2
		do j=i+1,NbPart-1
			PosPeriodicParts_ij = PosParts(i,:)-PosParts(j,:)-BoxL*ANINT((PosParts(i,:)-PosParts(j,:))/BoxL)
			RadialDistribution(RadialIndex) = SQRT(SUM(PosPeriodicParts_ij**2))
			RadialIndex=RadialIndex+1
		end do
	end do
END SUBROUTINE RADIAL_DISTRIBUTION

SUBROUTINE AMP_UPDATES(Amp,NbAccepted,NbCall)
	integer, intent(inout) :: NbAccepted, NbCall
	real, intent(inout) :: Amp
	if ((NbAccepted/REAL(NbCall)).LT.0.4) then !working for VC?
		Amp = Amp*0.9
	else if	((NbAccepted/REAL(NbCall)).GT.0.6) then
		Amp = Amp*1.1
	end if
	NbAccepted = 0
	NbCall = 0
END SUBROUTINE AMP_UPDATES

END MODULE SUBROUTINES

!----------MAIN----------!

PROGRAM LIQUID_POT
	USE SUBROUTINES
	IMPLICIT NONE
	integer :: Nb1Loop,Nb2Loop,NbPair,Nb12Loop,Nb3Loop
	integer :: i,j,k,l=0
	integer :: RadialIndex=0,NbPart,NbAcceptedPD=0,NbCallPD=0,TempRadialIndex=0
	real, parameter :: Kb=1.0
	real :: AmpVC=0.2,beta,Temp,Random
	real, dimension(:,:), allocatable :: PairsPot, PosParts
	real, dimension(:), allocatable :: RadialDistribution, ListPotential,ListPressure
	real :: Potential, BoxL, Rcut,AmpPD=0.1, MeanPotential, Pressure

	Nb1Loop=100
	Nb2Loop=50

	call READ_INPUTFILE(NbPart,BoxL,PosParts,Temp,Nb1Loop,Nb2Loop)


	Rcut = BoxL/2

	Nb3Loop=2*NbPart
	NbPair=((NbPart-1)*NbPart)/2
	Nb12Loop=Nb1Loop*Nb2Loop
	
	allocate(PairsPot(0:NbPart-1,0:NbPart-1))
	allocate(RadialDistribution(0:NbPair-1))
	allocate(ListPotential(0:Nb12Loop-1))
	allocate(ListPressure(0:Nb12Loop-1))

	beta = 1/(Kb*Temp)
	RadialIndex=0
	PairsPot=0
	RadialDistribution=0
    call POTENTIAL_INIT(NbPart,PosParts,PairsPot,BoxL,Potential,Rcut)
	call RANDOM_INIT()
	
	print*, Potential
	do i=0,Nb1Loop-1 !1Loop Updates of the displacement amplitude parameter
		do j=0,Nb2Loop-1 !2Loop Updates of the physical quantities
			do k=0,Nb3Loop-1 !3Loop Uncorrelation of physical observables
					call TRIAL_PARTICULE_DISPLACEMENT(NbPart,PosParts,PairsPot,BoxL,Potential,Rcut,&
							&AmpPD,beta,NbAcceptedPD,NbCallPD)
            end do
			call RADIAL_DISTRIBUTION(NbPart,PosParts,BoxL,RadialDistribution,Nb12Loop,NbPair)
			ListPotential(i*Nb2Loop+j) = Potential
			ListPressure(i*Nb2Loop+j) = (1/BoxL**3)*SUM(24*(&
		          &+(1.0/RadialDistribution)**6&
		          &-2*(1.0/RadialDistribution)**12))
		end do
		print*,'PD',NbCallPD,NbAcceptedPD,AmpPD
		call AMP_UPDATES(AmpPD,NbAcceptedPD,NbCallPD)
		print*,'Loading...',(i*100)/Nb1Loop,'%'
	end do


	MeanPotential = SUM(ListPotential(Nb12Loop/2-1:))/(Nb12Loop/2)
    Pressure = (NbPart/BoxL**3)/beta-(1/3.0)*SUM(ListPressure(Nb12Loop/2-1:))/(Nb12Loop/2)&
					&+(16.0/3.0)*4.d0*DATAN(1.D0)*(NbPart/BoxL**3)**2*((2.0/(3*Rcut**9))-(1.0/(Rcut**3)))
	call WRITE_OUTPUTFILE(NbPart,BoxL,Nb1Loop,Nb12Loop,NbPair,PosParts,RadialDistribution,MeanPotential&
		&,Pressure,ListPotential)
    print*,Pressure,MeanPotential

	deallocate(PosParts)
	deallocate(PairsPot)
	deallocate(RadialDistribution)
	deallocate(ListPotential)
END PROGRAM LIQUID_POT
