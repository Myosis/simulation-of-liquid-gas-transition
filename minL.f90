PROGRAM GEN_INPUTFILE
	IMPLICIT NONE
	integer,parameter :: NbPart=4*4**3
	integer :: i=0,j=0,x=0,y=0,z=0,k=0,NbPart1D
	real, dimension(0:NbPart-1,0:2) :: PosParts
	real :: epsi=1, sigma=1,Potential,r_ij,BoxL
	real, dimension(0:2) :: PosPeriodicParts_ij
	real, dimension(0:NbPart-1,0:NbPart-1) :: PairsPot
	PairsPot = 0
	Potential=0


	NbPart1D = NINT((NbPart/4)**(1.0/3.0))
	do k=0,20
		i=0
		BoxL=k*0.01+6.10
		do x=0,NbPart1D-1
			do y=0,NbPart1D-1
				do z=0,NbPart1D-1
					PosParts(i,0)=BoxL*(-0.5+x/REAL(NbPart1D))
					PosParts(i,1)=BoxL*(-0.5+y/REAL(NbPart1D))
					PosParts(i,2)=BoxL*(-0.5+z/REAL(NbPart1D))
					i=i+1
				end do
			end do
		end do
		PosParts(NbPart/4:NbPart/2-1,0) = PosParts(:NbPart/4-1,0) + 0.5*BoxL/NbPart1D
		PosParts(NbPart/4:NbPart/2-1,1) = PosParts(:NbPart/4-1,1) + 0.5*BoxL/NbPart1D
		PosParts(NbPart/4:NbPart/2-1,2) = PosParts(:NbPart/4-1,2)
		PosParts(NbPart/2:3*NbPart/4-1,0) = PosParts(:NbPart/4-1,0)
		PosParts(NbPart/2:3*NbPart/4-1,1) = PosParts(:NbPart/4-1,1) + 0.5*BoxL/NbPart1D
		PosParts(NbPart/2:3*NbPart/4-1,2) = PosParts(:NbPart/4-1,2) + 0.5*BoxL/NbPart1D
		PosParts(3*NbPart/4:NbPart-1,0) = PosParts(:NbPart/4-1,0) + 0.5*BoxL/NbPart1D
		PosParts(3*NbPart/4:NbPart-1,1) = PosParts(:NbPart/4-1,1)
		PosParts(3*NbPart/4:NbPart-1,2) = PosParts(:NbPart/4-1,2) + 0.5*BoxL/NbPart1D

		do i=0,NbPart-2
			do j=i+1,NbPart-1
				PosPeriodicParts_ij = PosParts(i,:)-PosParts(j,:)-BoxL*ANINT((PosParts(i,:)-PosParts(j,:))/BoxL)
				r_ij = SQRT(SUM((PosPeriodicParts_ij)**2)) 
				if (r_ij.LT.BoxL/2) then !Potential truncation?
					PairsPot(i,j) = (sigma/r_ij)**12-(sigma/r_ij)**6
					PairsPot(j,i) = PairsPot(i,j)
					Potential = Potential + PairsPot(i,j)
				else
					PairsPot(i,j) = 0
					PairsPot(j,i) = PairsPot(i,j)
				end if
			end do
		end do
		write(*,*) Potential,BoxL
		Potential=0
		PairsPot = 0
		PosParts = 0
	end do
END PROGRAM GEN_INPUTFILE
