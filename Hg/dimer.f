      subroutine V1si(r,vq,vj,v0,vt)
      implicit none
      double precision r,vq,vj,v0,vt
      double precision rcut3,C3,fcut3,e3,e6,D
      double precision a,rcut6,fcut6,C6,V1sg6s,V1sg6p
      double precision Re,Vu,z,zz,zinv,vg,e1p
      integer i
c      double precision lambdae,lambdar
      double precision acoef(13)
c     relativity and spin-orbit components

      rcut3=10.5D0
      fcut3=1.D0
      if (r.lt.rcut3) fcut3=dexp(-(rcut3/r-1.D0)**2)
      e3=fcut3/r**3

      e1p=0.186446D0

      if (relativistic) then

         e1p=0.245905d0

         acoef(1)=-392.d0
         acoef(2)=0.d0
         acoef(3)=534077.3126007207d0
         acoef(4)=-2.048297635930095d7
         acoef(5)=2.9552605727198446d8
         acoef(6)=-2.1155539832612858d9
         acoef(7)=8.125997490712963d9
         acoef(8)=-1.607981490556498d10
         acoef(9)=1.2894913865326828d10
         
c
c     X1Sg 6s adiabatic
c
         v1sg6s=0.D0
         do i=1,9
            a=acoef(i)/r**(i+5)
            v1sg6s=v1sg6s+a
         enddo
c
c     1Sg  6p adiabatic  (HG2POT1 lines 1:3, r>6)
c
         D=13.66898d0
         a=1.2d0
         c3=4.6825D0
         v1sg6p=D*dexp(-a*r)+C3*e3+e1p
c
c     1Su  6p adiabatic  (HG2POT1 lines 1:5)
c
         D=0.0189D0
         Re=5.5D0
         a=0.85D0
         C3=7.477D0
         vu=D*((1.d0-dexp(-a*(r-Re)))**2-1.D0)-C3*e3
c
c     1S Matrix elements
c
         D=-1.1d-3
         a=0.08D0
         z=D*(r**4)*dexp(-a*(r**2))
c      z=0.D0
         zz=z**2
         zinv=1.d0/(1.d0+zz)
         v0=(v1sg6s+zz*v1sg6p)*zinv
         vt=(v1sg6p-v1sg6s)*z*zinv/sqrt(2.d0)
         vg=(zz*v1sg6s+v1sg6p)*zinv-e1p
         
         vq=0.5d0*(vu+vg)
         vj=0.5d0*(vu-vg)

      else                      ! non relativistic

         acoef(1)=-1100.D0
         acoef(2)=0.0000000000000000D0
         acoef(3)=2.8492974083327814D6
         acoef(4)=-1.1299413271171196D8
         acoef(5)=1.8382967116783445D9
         acoef(6)=-1.5576165381432028D10
         acoef(7)=7.287938220493896D10
         acoef(8)=-1.792577619810004D11
         acoef(9)=1.8155145199668713D11
c
c     X1Sg 6s adiabatic
c
         v1sg6s=0.D0
         do i=1,9
            a=acoef(i)/r**(i+5)
            v1sg6s=v1sg6s+a
         enddo
c
c     1Sg  6p adiabatic  (HG2POT1 lines 1:3, r>6)
c
         D=1024.44D0
         a=2.217D0
         c3=4.876D0
         v1sg6p=D*dexp(-a*r)+C3*e3+e1p
c
c     1Su  6p adiabatic  (HG2POT1 lines 1:5)
c
         D=0.001569D0
         Re=6.12D0
         a=1.176D0
         C3=14.847D0
         vu=D*((1.d0-dexp(-a*(r-Re)))**2-1.D0)-C3*e3
c
c     1S Matrix elements
c
         D=-1.1d-3
         a=0.08D0
         z=D*(r**4)*dexp(-a*(r**2))
c      z=0.D0
         zz=z**2
         zinv=1.d0/(1.d0+zz)
         v0=(v1sg6s+zz*v1sg6p)*zinv
         vt=(v1sg6p-v1sg6s)*z*zinv/sqrt(2.d0)
         vg=(zz*v1sg6s+v1sg6p)*zinv-e1p
         
         vq=0.5d0*(vu+vg)
         vj=0.5d0*(vu-vg)

      endif

      return
      end
c_______________________________________________________________________

      subroutine V3si(r,vq,vj)
      implicit none
      double precision r,vq,vj
      double precision D,Re,a,vu,vg
c     relativity and spin-orbit components

c     matrix elements 3Q(vq) and 3J(vj), 3Sigma symmetry

      if (relativistic) then

c     3Su state  (HG2POT3 lines 1:4)

         D=0.0368D0
         Re=5.37D0
         a=0.8D0
         vu=D*((1.D0-dexp(-a*(r-Re)))**2-1.D0)

c     3Sg state  (HG2POT3 lines 1:2, r=>6)

         D=12.434D0
         a=0.988D0
         Vg=D*dexp(-a*r)

c     1S matrix elements

         vq=(vu+vg)/2.D0
         vj=(vu-vg)/2.D0

      else ! non relativistic

c     3Su state  (HG2POT3 lines 1:4)

         D=0.04297D0
         Re=5.3404D0
         a=0.8151D0
         vu=D*((1.D0-dexp(-a*(r-Re)))**2-1.D0)

c     3Sg state  (HG2POT3 lines 1:2, r=>6)

         D=5.533D0
         a=0.8134D0
         Vg=D*dexp(-a*r)

c     1S matrix elements

         vq=(vu+vg)/2.D0
         vj=(vu-vg)/2.D0

      endif

      return
      end
c_______________________________________________________________________

      subroutine V1pi(r,vq,vj)
      implicit none
      double precision r,vq,vj,vu,vg,Re
      double precision rcut3,C3,fcut3,e3,D,a
c     relativity and spin-orbit components

c     matrix elements 1Qtilde(vq) and 1Jtilde(vj), 1Pi symmetry

      rcut3=10.5D0
      fcut3=1.D0
      if (r.lt.rcut3) fcut3=dexp(-(rcut3/r-1.D0)**2)
      e3=fcut3/r**3

      if (relativistic) then

c     1Pu 6P adiabatic (HG2POT1 lines 1:4, r=>8)

         D=104.795D0
         a=1.6D0
         c3=4.6825D0
         vu=D*dexp(-a*r)+0.5D0*C3*e3

c     1Pg 6P adiabatic (HG2POT1 lines 1:6)

         D=0.065611D0
         Re=5.1D0
         a=0.77D0
         C3=2.81D0
         vg=D*((1.D0-dexp(-a*(r-Re)))**2-1.D0)-C3*e3

c     1p matrix elements

         vq=(vu+vg)/2.D0
         vj=(vu-vg)/2.D0

      else                      ! non relativistic

c     1Pu 6P adiabatic (HG2POT1 lines 1:4, r=>8)

         D=11452.6D0
         a=2.853D0
         c3=6.2067D0
         vu=D*dexp(-a*r)+0.5D0*C3*e3

c     1Pg 6P adiabatic (HG2POT1 lines 1:6)

         D=0.0685D0
         Re=5.29D0
         a=0.6789D0
         C3=4.935D0
         vg=D*((1.D0-dexp(-a*(r-Re)))**2-1.D0)-C3*e3

c     1p matrix elements

         vq=(vu+vg)/2.D0
         vj=(vu-vg)/2.D0

      endif

      return
      end
c_______________________________________________________________________

      subroutine V3pi(r,vq,vj)
      implicit none
      double precision r,vq,vj
      double precision D,Re,a,vu,vg
c     relativity and spin-orbit components
c     matrix elements 3Qtilde(vq) and 3Jtilde(vj), 3Pi symmetry

      if (relativistic) then

c     3Pg state (HG2POT3 lines 1:5)

         D=0.04697D0
         Re=5.14D0
         a=0.87D0
         vg=D*((1.D0-dexp(-a*(r-Re)))**2-1.D0)

c     3Pu state (HG2POT3 lines 1:3)

         D=1085.52D0
         a=2.196D0
         vu=D*dexp(-a*r)

c     3P matrix elements

         vq=(vu+vg)/2.D0
         vj=(vu-vg)/2.D0

      else ! non relativistic

c     3Pg state (HG2POT3 lines 1:5)

         D=0.03917D0
         Re=5.625D0
         a=0.7133D0
         vg=D*((1.D0-dexp(-a*(r-Re)))**2-1.D0)

c     3Pu state (HG2POT3 lines 1:3)

         D=382.25D0
         a=1.882D0
         vu=D*dexp(-a*r)

c     3P matrix elements

         vq=(vu+vg)/2.D0
         vj=(vu-vg)/2.D0

      endif

      return
      end
