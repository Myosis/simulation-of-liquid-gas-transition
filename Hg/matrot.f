      subroutine matrot(rot,natoms,xij,yij,zij,dist,i,j)
      implicit none
      integer nmax,natoms
      parameter(nmax=129)
      double precision xij(nmax,nmax),yij(nmax,nmax),zij(nmax,nmax)
      double precision rot(3,3),xx,yy,zz,dist
      double precision RO,costet,sintet,cosphi,sinphi,ROmin
      integer i,j
      parameter(ROmin=1.D-10)

      xx=xij(i,j)/dist
      yy=yij(i,j)/dist
      zz=zij(i,j)/dist
      RO=dsqrt(xx**2+yy**2)
      costet=zz
      sintet=RO
      cosphi=1.D0
      sinphi=0.D0
      if (RO.ge.ROmin) then
         cosphi=XX/RO
         sinphi=YY/RO
      endif

      ROT(1,1)=COSTET*COSPHI
      ROT(2,1)=-SINPHI
      ROT(3,1)=SINTET*COSPHI

      ROT(1,2)=COSTET*SINPHI
      ROT(2,2)=COSPHI
      ROT(3,2)=SINTET*SINPHI

      ROT(1,3)=-SINTET
      ROT(2,3)=0.D0
      ROT(3,3)=COSTET

      return
      end
