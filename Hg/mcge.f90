MODULE SUBROUTINES
    USE DIM_MODULE
	IMPLICIT NONE
	! n = number, up = update, av = average
    integer :: Ntot,nvar,nVol,nswap,step,ncy,nupVol,b,a, nsub, acvm,adjstv,LC,nCP1,nupDens, nCycle, nsave
    integer, dimension(1:2) :: creat_box,remov_box,ipull_box,nup_box,adjst_box
    integer, dimension(1:2) :: N, N_0, tryin_box, nCP, acin_box, step_box,accm_box
    real(8) :: ran,ran3(3),dvmax,ratiov,V,Xij,Yij,Zij,VolTotal
    real(8) :: PElrc,beta,Temp,DensCP,DV_in, DV_out,PEMove
    real(8), dimension(1:2) :: ratioc_box,box_side,half_box_side,Dens,Vol,drmax
    real(8), dimension(1:2) :: PElrc_box, rcut, Dens_0
    real(8), dimension(1:2) :: PE_box, PEN_box, AVPE_box
    real(8), dimension(1:2) :: AVDens_box, AVVol_box, AVN_box, xin_box, yin_box, zin_box
    real(8), dimension(1:2) :: accp_box, AVAcT_box, AVEx_box, ChemPot_box, ACChemPot_box
    real(8), dimension(1:2) :: ACChemPot2_box, AVChemPot_box, AVrcut
    real(8), dimension(:,:), allocatable :: X,Y,Z
	contains

!----------INITIALISATION|LECTURE DES DONNEES----------!

SUBROUTINE INIT()
	CHARACTER(len=32) :: NbPartString1,NbPartString2,TempString,DensityString1,DensityString2,RelatString

	!USER INPUT
	CALL GET_COMMAND_ARGUMENT(1, TempString)
	CALL GET_COMMAND_ARGUMENT(2, NbPartString1)
	CALL GET_COMMAND_ARGUMENT(3, NbPartString2)
	CALL GET_COMMAND_ARGUMENT(4, DensityString1)
    CALL GET_COMMAND_ARGUMENT(5, DensityString2)
    CALL GET_COMMAND_ARGUMENT(6, RelatString)

	IF (LEN_TRIM(TempString).EQ.0) then
		STOP 'No Temperature as argument'
	else
		TempString = TRIM(TempString)
		read(TempString,*) Temp
    end if
    
	IF (LEN_TRIM(RelatString).EQ.0) then
		STOP 'No Relat as argument'
	else
		RelatString = TRIM(RelatString)
        if (RelatString.EQ.'0') then
            relativistic=.false.
        else
            relativistic=.true.
        end if
        periodic=.true.
	end if

	IF ((LEN_TRIM(NbPartString1).EQ.0).OR.(LEN_TRIM(NbPartString2).EQ.0)) then
		STOP 'No Number of Particules as argument'
	else
		NbPartString1 = TRIM(NbPartString1)
		NbPartString2 = TRIM(NbPartString2)
		read(NbPartString1,*) N_0(1)
		read(NbPartString2,*) N_0(2)
		Ntot=N_0(1)+N_0(2)
	end if

	IF ((LEN_TRIM(DensityString1).EQ.0).OR.(LEN_TRIM(DensityString2).EQ.0)) then
		STOP 'No Density as argument'
	else
		DensityString1 = TRIM(DensityString1)
		DensityString2 = TRIM(DensityString2)
		read(DensityString1,*) Dens_0(1)
		read(DensityString2,*) Dens_0(2)
	end if

	allocate(X(1:2,0:NTot))
	allocate(Y(1:2,0:NTot))
	allocate(Z(1:2,0:NTot))
	X = 0
	Y = 0
	Z = 0
	!@@@@@@@@@@@@@@
	N_0=Ntot/2
	DensCP=0.0015
	call DIAGRAM_INIT()
    print*,'N0',N_0,'Dens0',Dens_0,'relat',relativistic
	!@@@@@@@@@@@@@@
	N = N_0
	Dens = Dens_0
	Vol = N/Dens
	box_side = Vol**(1.0/3.0)
	half_box_side = box_side/2
	drmax = box_side*0.02
	dvmax = MINVAL(Vol)*0.02
	beta = 1.0/(3.1668114e-6*Temp)
	rcut = half_box_side

	b=1
	call initialConfiguration()
	call POTENTIAL(b)
	PE_box(b) = V
	b=2
	call initialConfiguration()
	call POTENTIAL(b)
	PE_box(b) = V
	print*,PE_box

	AVDens_box = Dens
	AVN_box = N 
	AVPE_box = PE_box/N
	step=0
	step_box=0
	tryin_box=0
	nup_box=0
	nupVol=0
	creat_box=0
	ratioc_box=0
	ratiov=0
	accm_box=0
	acvm=0
	AVVol_box=0
	acin_box=0
	AVAcT_box=0
	AVEx_box=0
	ACChemPot_box=0
	ACChemPot2_box=0
	ChemPot_box=0
	nCP=0
	LC=0
	AVrcut=0
	nswap=Ntot
	ncy=1e4
	adjst_box=1e3
	adjstv=5e2
	nsub=200
	nVol=1
	nvar = Ntot+nVol+nswap
	VolTotal=Vol(1)+Vol(2)
	nCycle = 2e4
	nsave = 2e3
END SUBROUTINE INIT

SUBROUTINE DIAGRAM_INIT()
	real(8) :: DensPrime
	Dens_0(1) = (1.25e-7)*Temp-(5e-5)
	Dens_0(2) = -(1.3e-6)*Temp+(42.6e-4)
	DensPrime = 1.0*Ntot/((N_0(1)/Dens_0(1))+(N_0(2)/Dens_0(2)))
	do while (DensPrime.LT.DensCP)
		N_0(1)=N_0(1)-1
		N_0(2)=Ntot-N_0(1)
		DensPrime = 1.0*Ntot/((N_0(1)/Dens_0(1))+(N_0(2)/Dens_0(2)))
	end do
	if (N_0(1).EQ.0) then
		N_0(1)=1
		N_0(2)=Ntot-1
	end if
	DensPrime = 1.0*Ntot/((N_0(1)/Dens_0(1))+(N_0(2)/Dens_0(2)))
	print*,'DensTot ',DensPrime
END SUBROUTINE DIAGRAM_INIT

!----------BOUCLE MONTE CARLO----------!

SUBROUTINE MAIN()
	integer :: i,j
    do i=0, nCycle
		step=step+1
		do j=0,nvar-1
			call RANDOM_NUMBER(ran)
			if (Ran .LT. 1.0*N(1)/nvar) then
				b=1
				call TRIAL_PARTICULE_DISPLACEMENT()
			else if (Ran .LT. 1.0*Ntot/nvar) then
				b=2
				call TRIAL_PARTICULE_DISPLACEMENT()
			else if (Ran .LT. 1.0*(NTot+nVol)/nvar) then
				call TRIAL_VOLUME_CHANGE()
			else if (Ran .LT. (NTot+nVol+0.5*nswap)/nvar) then
				b=1
				a=2
				call TRIAL_PARTICULE_EXCHANGE()
			else
				b=2
				a=1
				call TRIAL_PARTICULE_EXCHANGE()
			end if
		end do
		if (MODULO(step,nsave).EQ.0) then
            call SAVE_STATE()
		end if
		if (step.GT.ncy) then
			call statistics()
			print*,AVPE_box,AVDens_box,AVN_box,AVChemPot_box
			write(66,*) step_box,accm_box,nupVol,acvm,tryin_box,creat_box
		end if
		write(65,*) PE_box,Dens,N
	end do
END SUBROUTINE MAIN

SUBROUTINE POTENTIAL(boxID)
	integer, intent(in) :: boxID
	real(8), dimension(0:(Ntot+1)*12,0:(Ntot+1)*12) :: HaR,HaI
	real(8) :: eigVal(0:N(boxID)*12), RWork(0:7*N(boxID)*12+6)
	integer :: NbEig,Info,IFail(0:N(boxID)*12),IWork(0:5*N(boxID)*12+4),LWork,LDZ=1
	complex(8) :: Work(0:2*N(boxID)*12+1),ZZ(1,0:N(boxID)*12)
	LWork=2*(N(boxID)*12+1)
	call hamilton(N(boxID),X(boxID,:),Y(boxID,:),Z(boxID,:),HaR,HaI,box_side(boxID))
	if (.NOT.ovrlap) then
	call zheevx('N','I','U',N(boxID)*12+1,CMPLX(HaR,HaI,8),12*(Ntot+1)+1,0,0,1,1&
        &,0,NbEig,eigVal,ZZ,LDZ,Work,LWork,RWork,IWork,IFail,Info)
	V=eigVal(0)
	end if
END SUBROUTINE POTENTIAL

SUBROUTINE initialConfiguration()
	integer :: nucpd,i,lx,ly,lz,j
	real(8) :: unit_cell_side
	real(8), dimension(0:3) :: xc = (/0.0,0.0,0.5,0.5/)
	real(8), dimension(0:3) :: yc = (/0.0,0.5,0.5,0.0/)
	real(8), dimension(0:3) :: zc = (/0.0,0.5,0.0,0.5/)
	nucpd=0
	if (N(b) .LT. 4+1) nucpd=1
	if ((N(b) .GT. 4)  .AND. (N(b) .LT. 32+1)) nucpd=2
	if ((N(b) .GT. 32)  .AND. (N(b) .LT. 108+1)) nucpd=3
	if ((N(b) .GT. 108) .AND. (N(b) .LT. 256+1)) nucpd=4
	if ((N(b) .GT. 256) .AND. (N(b) .LT. 500+1)) nucpd=5
	if ((N(b).GT.500) .AND. (N(b) .LT. 864+1)) nucpd=6
	if (N(b) .GT. 864) STOP 'Too much particules in one box'
	unit_cell_side = box_side(b)/nucpd
	i=0
	do lx=1,nucpd
		do ly=1,nucpd
			do lz=1,nucpd
				do j=0,3
					if (i.LT.N(b)) then
						X(b,i) =(lx - 0.75 + xc(j))*unit_cell_side - half_box_side(b)
				    	Y(b,i) =(ly - 0.75 + yc(j))*unit_cell_side - half_box_side(b)
				    	Z(b,i) =(lz - 0.75 + zc(j))*unit_cell_side - half_box_side(b)
				    	i=i+1
				    end if
				end do 
			end do 
		end do 
	end do
END SUBROUTINE initialConfiguration

!----------ACTION 1 DEPLACEMENT D'UNE PARTICULE----------!

SUBROUTINE TRIAL_PARTICULE_DISPLACEMENT()
	integer :: i
	real(8) :: PEOLD,PENEW, XOLD,YOLD,ZOLD
	real(8) :: delPE,delPEB, XNEW,YNEW,ZNEW
	if (N(b).GT.1) then
		step_box(b) = step_box(b) +1
		call RANDOM_NUMBER(ran)
		i = INT(ran*N(b))
		PEOLD = PE_box(b)
		call RANDOM_NUMBER(Ran3)
		XNEW = X(b,i) + (2.0*ran3(1)-1) * drmax(b)
		YNEW = Y(b,i) + (2.0*ran3(2)-1) * drmax(b)
		ZNEW = Z(b,i) + (2.0*ran3(3)-1) * drmax(b)
        XOLD = X(b,i)
        YOLD = Y(b,i)
        ZOLD = Z(b,i)
        X(b,i) = XNEW
        Y(b,i) = YNEW
        Z(b,i) = ZNEW
        
        call POTENTIAL(b)
        PENEW = V
		delPE = PENEW - PEOLD
		delPEB  = beta * delPE
		call RANDOM_NUMBER(Ran)
		if ((delPEB.LT.0.0).OR.(EXP(-delPEB).GT.ran)) then
			accm_box(b) = accm_box(b)+1
    		PE_box(b)  = PE_box(b)+delPE
            PEN_box(b) = PE_box(b)/N(b)
        else
			X(b,i) = XOLD
			Y(b,i) = YOLD
			Z(b,i) = ZOLD
		end if
		nup_box(b) = nup_box(b)+1
		if (MODULO(nup_box(b),adjst_box(b)).EQ.0) then
			ratioc_box(b) = 1.0*accm_box(b)/adjst_box(b)
			if (ratioc_box(b) .GT. 0.5) then
				drmax(b) = drmax(b) * 1.05
			else
				drmax(b) = drmax(b) * 0.95
			end if
			if (drmax(b) > half_box_side(b)) drmax(b)=half_box_side(b)
			accm_box(b) = 0.0
		end if
	else
		PEN_box(b) = 0.0
	end if
END SUBROUTINE TRIAL_PARTICULE_DISPLACEMENT

!----------ACTION 2 ECHANGE DE VOLUME----------!

SUBROUTINE TRIAL_VOLUME_CHANGE()
	real(8) :: delVol, delvol_fact, delbetaPEVol
	real(8), dimension(2) :: Vol_new,box_sideNew,ratbox,rrbox,PEOLD_box, Vol_OLD
	real(8), dimension(2) :: delPE_box, box_side_OLD
	real(8), dimension(1:2,0:NTot-1) :: XOLD,YOLD,ZOLD
	
	call RANDOM_NUMBER(ran)
	Vol_new(1) = Vol(1) + (2.0*ran-1)*dvmax
	Vol_new(2) = VolTotal - Vol_new(1)
	
	if ((Vol_new(1).GT.0.0).AND.(Vol_new(2).GT.0.0)) then
		box_sideNew = Vol_new**(1.0/3.0)
		ratbox = box_side/box_sideNew
		rrbox = 1.0/ratbox
        PEOLD_box = PE_box
        Vol_OLD = Vol
        box_side_OLD = box_side
        
        XOLD = X
        YOLD = Y
        ZOLD = Z
        
        X(1,:) = X(1,:)*rrbox(1)
        Y(1,:) = Y(1,:)*rrbox(1)
        Z(1,:) = Z(1,:)*rrbox(1)
        X(2,:) = X(2,:)*rrbox(2)
        Y(2,:) = Y(2,:)*rrbox(2)
        Z(2,:) = Z(2,:)*rrbox(2)

        box_side = box_sideNew
        half_box_side = box_side/2.0
        rcut = half_box_side
        vol = Vol_new
        Dens = N/Vol

		call POTENTIAL(1)
		PE_box(1) = V
        call POTENTIAL(2)
		PE_box(2) = V

        delPE_box = PE_box - PEOLD_box
        delVol = Vol_New(1) - Vol(1)
        delVol_fact = -N(1)*LOG(Vol(1)/Vol_OLD(1))-N(2)*LOG(Vol(2)/Vol_OLD(2))
        delbetaPEVol = beta*(delPE_box(1)+delPE_box(2))+delVol_fact
		call RANDOM_NUMBER(ran)
		if ((delbetaPEVol.LT.0.0).OR.(EXP(-delbetaPEVol).GT.ran)) then
			acvm = acvm +1
        else
            PE_box = PEOLD_box
            Vol = Vol_OLD
            box_side = box_side_OLD
    		half_box_side = box_side/2.0
    		Rcut = half_box_side
    		X = XOLD
    		Y = YOLD
    		Z = ZOLD
		end if
		Dens = N/Vol
		nupDens = nupDens +1
		if (N(1).GT.1) then
    		PEN_box(1) = (PE_box(1))/N(1)
    	else
			PEN_box(1) = 0.0
    	end if
		if (N(2).GT.1) then
    		PEN_box(2) = (PE_box(2))/N(2)
    	else
			PEN_box(2) = 0.0
    	end if
	end if
	nupVol = nupVol+1
	if (MODULO(nupVol,adjstv).EQ.0) then
		ratiov = 1.0*acvm/adjstv
		if (ratiov .GT. 0.5) then
			dvmax = dvmax * 1.05
		else
			dvmax = dvmax * 0.95
		end if
		acvm = 0.0
	end if
END SUBROUTINE TRIAL_VOLUME_CHANGE

!----------ACTION 3 ECHANGE DE PARTICULES----------!

SUBROUTINE TRIAL_PARTICULE_EXCHANGE()
	real(8) :: delInOut, XOLD, YOLD, ZOLD
	real(8), dimension(2) :: delPE_box
	if (N(b).LT.Ntot) then
		tryin_box(b) = tryin_box(b)+1
		call particuleIn()
		if (.NOT.(ovrlap)) then
			call particuleOut(XOLD,YOLD,ZOLD)
			acin_box(b) = acin_box(b)+1
			delPE_box(b) = DV_in
			delPE_box(a) = DV_out
			accp_box(b) = accp_box(b) + EXP(-beta*delPE_box(b))
			delInOut = beta*(delPE_box(b)+delPE_box(a))+LOG((Vol(a)*(N(b)))/(Vol(b)*(N(a)+1))) !!!Nb(b)+1
			call RANDOM_NUMBER(ran)
			if ((delInOut.LT.0.0).OR.(EXP(-delInOut).GT.ran)) then
				creat_box(b) = creat_box(b) +1
	    		PE_box(b)  = PE_box(b)  +DV_in
	    		remov_box(a) = remov_box(a) +1
				PE_box(a)  = PE_box(a)  +DV_out
			else
				N(a)=N(a)+1
				X(a,N(a)-1)=XOLD
				Y(a,N(a)-1)=YOLD
				Z(a,N(a)-1)=ZOLD
				N(b)=N(b)-1
				X(b,N(b)) = 0
				Y(b,N(b)) = 0
				Z(b,N(b)) = 0
			end if
		else
			N(b)=N(b)-1
			X(b,N(b)) = 0
			Y(b,N(b)) = 0
			Z(b,N(b)) = 0
    	end if
    	Dens = N/Vol 
    	nupDens = nupDens+1
		if (N(1).GT.1) then
    		PEN_box(1) = PE_box(1)/N(1)
    	else
			PEN_box(1) = 0.0
    	end if
		if (N(2).GT.1) then
    		PEN_box(2) = PE_box(2)/N(2)
    	else
			PEN_box(2) = 0.0
    	end if
    else
    	tryin_box(b)=tryin_box(b)+1
		call particuleIn()
		N(b)=N(b)-1
		X(b,N(b)) = 0
		Y(b,N(b)) = 0
		Z(b,N(b)) = 0
    	if (.NOT.(ovrlap)) then
    		acin_box(b) = acin_box(b) +1
    		delPE_box(b) = DV_in
    		accp_box(b) = accp_box(b) + EXP(-beta*delPE_box(b))
        end if
	end if
	ovrlap = .False.
END SUBROUTINE TRIAL_PARTICULE_EXCHANGE

SUBROUTINE particuleIn()
	ovrlap = .False.
	call RANDOM_NUMBER(ran3)
	xin_box(b) = ran3(1)*box_side(b)-half_box_side(b)
	yin_box(b) = ran3(2)*box_side(b)-half_box_side(b)
	zin_box(b) = ran3(3)*box_side(b)-half_box_side(b)
	N(b)=N(b)+1
	X(b,N(b)-1) = xin_box(b)
	Y(b,N(b)-1) = yin_box(b)
	Z(b,N(b)-1) = zin_box(b)
	if (N(b)-1.GT.0) then
		call POTENTIAL(b)
		DV_in = - (PE_box(b) - V)
	else
	    DV_in = 0.0
	end if
END SUBROUTINE particuleIn

SUBROUTINE particuleOut(XOLD,YOLD,ZOLD)
	real(8), intent(out) :: XOLD, YOLD, ZOLD
	call RANDOM_NUMBER(ran)
	ipull_box(a) = INT(N(a)*ran)
	XOLD = X(a,ipull_box(a))
	YOLD = Y(a,ipull_box(a))
	ZOLD = Z(a,ipull_box(a))
	X(a,ipull_box(a)) = X(a,N(a)-1)
	Y(a,ipull_box(a)) = Y(a,N(a)-1)
	Z(a,ipull_box(a)) = Z(a,N(a)-1)
	N(a)=N(a)-1
	if (N(a).GT.1) then
		call POTENTIAL(a)
		DV_out = - (PE_box(a) - V)
	else
	    DV_out = 0.0
	end if
END SUBROUTINE particuleOut

SUBROUTINE statistics()
	integer :: LC1
	LC = LC+1
	LC1 = LC-1
	AVPE_box=(AVPE_box*LC1+PEN_box)/LC
	AVDens_box=(AVDens_box*LC1+Dens)/LC
	AVN_box=(AVN_box*LC1+N)/LC
	AVVol_box=(AVVol_box*LC1+Vol)/LC
	AVrcut=(AVrcut*LC1+rcut)/LC
	AVAcT_box=(1.0*acin_box/tryin_box)* 100
	AVEx_box=(1.0*creat_box/tryin_box)* 100
	if ((accp_box(1) .GT. 0.0) .AND. (AVDens_box(1) .GT. 0.0)) then
	  ChemPot_box(1)=(1.0/beta)*(LOG(AVDens_box(1))-LOG(1.0*accp_box(1)/tryin_box(1)))
	end if
	if ((accp_box(2) .GT. 0.0) .AND. (AVDens_box(2) .GT. 0.0)) then
	  ChemPot_box(2)=(1.0/beta)*(LOG(AVDens_box(2))-LOG(1.0*accp_box(2)/tryin_box(2)))
	end if
	nCP1 = nCP1 + 1;
	ACChemPot_box = ACChemPot_box +ChemPot_box
	ACChemPot2_box = ACChemPot2_box +ChemPot_box * ChemPot_box
	AVChemPot_box = ACChemPot_box/nCP1
	!FLChemPot_box = SQRT(ABS((ACChemPot2_box/nCP1)-(AVChemPot_box*AVChemPot_box)))
	if (LC.EQ.1) then
		open(34, file="PEEvol.txt",access='sequential', form='formatted', status="unknown", action="write")
		open(35, file="VolEvol.txt",access='sequential', form='formatted', status="unknown", action="write")
		open(36, file="NEvol.txt",access='sequential', form='formatted', status="unknown", action="write")
	end if
	write(34,*) PEN_box
	write(35,*) Vol/VolTotal
	write(36,*) 1.0*N/Ntot
		
END SUBROUTINE statistics

SUBROUTINE SAVE_STATE()
	integer :: i,j
	CHARACTER(len=3) :: WhichParts = 'Hg '

	open(12, file="gibbsHg.of",access='sequential', form='formatted', status="unknown", action="write")
	write(12,*) Ntot,N(1),N(2)
	write(12,*) box_side(1),box_side(2)
	do i=0,N(1)-1
		write(12,113) WhichParts,X(1,i),Y(1,i),Z(1,i)
		113 format (A3,3ES15.7)
	end do
	do j=0,N(2)-1
		write(12,114) WhichParts,X(2,j),Y(2,j),Z(2,j)
		114 format (A3,3ES15.7)
	end do
	write(12,*) 'step',step
	close(12)
END SUBROUTINE SAVE_STATE

SUBROUTINE RANDOM_INIT()
	integer :: k
	integer, dimension(0:7) :: values
	integer, dimension(:), allocatable :: seed
	call date_and_time(values=values)
	call RANDOM_SEED(size=k)
	allocate(seed(1:k))
	seed(:) = values(7)
	call RANDOM_SEED(put=seed)
END SUBROUTINE RANDOM_INIT

END MODULE SUBROUTINES

!----------LANCEMENT DES SUBROUTINES----------!

PROGRAM LIQUID_POT
	USE SUBROUTINES
	IMPLICIT NONE

	call RANDOM_INIT()
	call INIT()
	call MAIN()

END PROGRAM LIQUID_POT
