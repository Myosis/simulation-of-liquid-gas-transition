      subroutine hamilton(natoms,xlist,ylist,zlist,har,hai,boxl)
      implicit none
      integer nmax,natoms,natz
      parameter(nmax=129,natz=12*nmax+1)
      double precision xlist(nmax),ylist(nmax),zlist(nmax)
      double precision vp(natz),vecr(natz,natz),veci(natz,natz)
      double precision vecr_gs(natz),veci_gs(natz)
      double precision energy,boxl
      double precision Har(natz,natz),Hai(natz,natz)
      double precision Har_save(natz,natz),Hai_save(natz,natz)
      double precision SOar(12,12),SOai(12,12),WA(natz)
      double precision WB(natz),WC(natz),rot(3,3)
      integer ind(natz),is,js
      integer i,j,nbas,indf,ii,l,k,jj,idum,ib,jb,iisave,ik,il,ifail
      double precision zeta,e3p,e1p,V1qs,V1js,V0,Vt,v3qs,v3js,v1qp,v1jp
      double precision v3qp,v3jp,egrnd
      double precision xij(nmax,nmax),yij(nmax,nmax),zij(nmax,nmax)
      double precision rij(nmax,nmax)
      double precision dist,rcut,e3,e1,v3,v1,vsp,zxa,zxb

      rcut=100.D0
c     array of distances

      do i=1,natoms
         do j=1,i-1
            xij(i,j)=xlist(j)-xlist(i)
            yij(i,j)=ylist(j)-ylist(i)
            zij(i,j)=zlist(j)-zlist(i)

            if (periodic) then
               xij(i,j)=xij(i,j)-boxl*anint(xij(i,j)/boxl)
               yij(i,j)=yij(i,j)-boxl*anint(yij(i,j)/boxl)
               zij(i,j)=zij(i,j)-boxl*anint(zij(i,j)/boxl)
            endif

            rij(i,j)=sqrt(xij(i,j)**2+yij(i,j)**2+zij(i,j)**2)
            xij(j,i)=-xij(i,j)
            yij(j,i)=-yij(i,j)
            zij(j,i)=-zij(i,j)
            rij(j,i)= rij(i,j)
c           it rejects too close particles and avoids
c           incalculable potential

            if (rij(i,j).LT.0.5) then
               ovrlap=.True.
               print*,'uncalculable'
               return
            end if
         enddo
      enddo
      nbas=12*natoms+1
      indf=nbas
      do i=1,natoms
         ind(i)=(i-1)*12
      enddo
      if (relativistic) then
         e3p=0.190403D0
         e1p=0.245905d0
      else
         e3p=0.124424D0
         e1p=0.186446D0
      endif

      do i=1,nbas
         do j=1,nbas
            Har(i,j)=0.D0
            Hai(i,j)=0.D0
         enddo
      enddo

      if (relativistic) then
         zeta=0.01943D0
         do i=1,12
            do j=1,12
               soar(i,j)=0.D0
               soai(i,j)=0.D0
            enddo
         enddo
      endif

c     ground state

      egrnd=0.D0
      do i=2,natoms
         do j=1,i-1
            dist=rij(i,j)
            call V1si(dist,V1qs,V1js,V0,Vt)
            egrnd=egrnd+V0
         enddo
      enddo
      Har(indf,indf)=egrnd
      do i=1,natoms
         ii=ind(i)-4
         do j=1,3
            ii=ii+4
            do l=1,3
               Har(ii+l,ii+l)=E3p+egrnd
            enddo
            Har(ii+4,ii+4)=E1p+egrnd
         enddo
      enddo
c     Excited states: diagonal blocks

      do i=1,natoms
         do j=1,natoms
            if (i.ne.j) then
               dist=rij(i,j)
               if (dist.lt.rcut) then
                  call matrot(rot,natoms,xij,yij,zij,dist,i,j)
               endif

c     diatomic curves

               call v1si(dist,v1qs,v1js,v0,vt)
               call v3si(dist,v3qs,v3js)
               call v1pi(dist,v1qp,v1jp)
               call v3pi(dist,v3qp,v3jp)

c     diagonal blocks for excited states

               do k=1,3
                  ii=ind(i)+(k-1)*4
                  do l=k,3
                     jj=ind(i)+(l-1)*4
                     e3=rot(1,k)*rot(1,l)*v3qp
     &                    +rot(2,k)*rot(2,l)*v3qp
     &                    +rot(3,k)*rot(3,l)*v3qs
                     e1=rot(1,k)*rot(1,l)*v1qp
     &                    +rot(2,k)*rot(2,l)*v1qp
     &                    +rot(3,k)*rot(3,l)*v1qs
                     if (ii.eq.jj) then
c                        print*,e1,e3,v0,e3-v0,e1-v0
                        e3=e3-v0
                        e1=e1-v0
                     endif
                     do idum=1,3
                        Har(ii+idum,jj+idum)=Har(ii+idum,jj+idum)+e3
                     enddo
                     Har(ii+4,jj+4)=Har(ii+4,jj+4)+e1
                  enddo
               enddo
            endif
         enddo
      enddo

c     extradiagonal blocks for excited states

      do i=1,natoms
         do j=i+1,natoms
            dist=rij(i,j)
            if (dist.lt.rcut) then
               call matrot(rot,natoms,xij,yij,zij,dist,i,j)
            endif

c     diatomic curves

            call v1si(dist,v1qs,v1js,v0,vt)
            call v3si(dist,v3qs,v3js)
            call v1pi(dist,v1qp,v1jp)
            call v3pi(dist,v3qp,v3jp)
            
            do k=1,3
               ii=ind(i)+(k-1)*4
               do l=k,3
                  jj=ind(j)+(l-1)*4

                  v3=rot(1,k)*rot(1,l)*v3jp
     &                 +rot(2,k)*rot(2,l)*v3jp
     &                 +rot(3,k)*rot(3,l)*v3js
                  v1=rot(1,k)*rot(1,l)*v1jp
     &                 +rot(2,k)*rot(2,l)*v1jp
     &                 +rot(3,k)*rot(3,l)*v1js
                  do idum=1,3
                     Har(ii+idum,jj+idum)=v3
                  enddo
                  Har(ii+4,jj+4)=v1

c     symmetrization in nondiagonal blocks
                  
                  if (k.ne.l) then
                     ib=ind(i)+(l-1)*4
                     jb=ind(j)+(k-1)*4
                     do idum=1,4
                        is=ib+idum
                        js=jb+idum
                        if (is.gt.js) then
                           iisave=is
                           is=js
                           js=iisave
                        endif
                        if (idum.le.3) then
                           Har(is,js)=v3
                        else
                           Har(is,js)=v1
                        endif
                     enddo
                  endif
               enddo
            enddo
         enddo
      enddo

c     couplings ground state/excited state

      do i=1,natoms
         do j=i+1,natoms
            dist=rij(i,j)
            call v1si(dist,v1qs,v1js,v0,vt)
            if (dist.lt.rcut) then
               call matrot(rot,natoms,xij,yij,zij,dist,i,j)
            endif

            do k=1,3
               vsp=rot(3,k)*vt
               ii=ind(i)+(k-1)*4+4
               jj=ind(j)+(k-1)*4+4
               Har(ii,indf)=Har(ii,indf)+vsp
               Har(jj,indf)=Har(jj,indf)-vsp
            enddo
         enddo
      enddo

c     end of loop on pairs

      if (relativistic) then
c     spin-orbit
         zxa=0.5D0*zeta
         zxb=zxa/sqrt(2.D0)
         soar(1,11)=-zxb
         soar(1,12)=zxb
         soar(2,11)=-zxb
         soar(2,12)=-zxb
         soar(3,9)=zxb
         soar(3,10)=zxb
         soar(4,9)=-zxb
         soar(4,10)=zxb 
         soai(1,5)=-zxa
         soai(2,6)=zxa
         soai(4,7)=-zxa
         soai(3,8)=-zxa
         soai(5,11)=zxb
         soai(5,12)=-zxb 
         soai(6,11)=-zxb
         soai(6,12)=-zxb
         soai(7,9)=zxb
         soai(7,10)=-zxb
         soai(8,9)=-zxb
         soai(8,10)=-zxb
         do i=1,12
            do j=i,12
               soar(j,i)=soar(i,j)
               soai(j,i)=-soai(i,j)
            enddo
         enddo
         
         do i=1,natoms
            do k=1,12
               do l=1,12
                  ik=ind(i)+k
                  il=ind(i)+l
                  Hai(ik,il)=Hai(ik,il)+soai(k,l)
                  Har(ik,il)=Har(ik,il)+soar(k,l)
               enddo
            enddo
         enddo
      endif

      do i=1,nbas
         do j=i,nbas
            har(j,i)=har(i,j)
            if (relativistic) hai(j,i)=-hai(i,j)
         enddo
      enddo
      return
      end
