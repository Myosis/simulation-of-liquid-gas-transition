        double precision FUNCTION RAN(nrandom)

        INTEGER     L, C, M
        PARAMETER ( L = 1029, C = 221591, M = 1048576 )

        INTEGER     nrandom

        nrandom = MOD ( nrandom * L + C, M )
        RAN = 1.d0*nrandom/M

        RETURN
        END
