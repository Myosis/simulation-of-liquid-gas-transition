      function henergy(natoms,har,hai,VP,vec_gs)
      implicit none
      integer nmax,natoms,natz
      parameter(nmax=129,natz=12*nmax+1)
      double precision x(nmax),y(nmax),z(nmax)
      double precision vp(natz),vecr(natz,natz),veci(natz,natz)
      double precision vecr_gs(natz),veci_gs(natz)
      double precision henergy
      double precision Har(natz,natz),Hai(natz,natz)
      double precision Har_save(natz,natz),Hai_save(natz,natz)
      double precision SOar(12,12),SOai(12,12),WA(natz)
      double precision WB(natz),WC(natz),rot(3,3)
      integer ind(natz)
      integer i,j,nbas,indf,ii,l,k,jj,idum,ib,jb,iisave,ik,il,ifail
      double precision zeta,e3p,e1p,V1qs,V1js,V0,Vt,v3qs,v3js,v1qp,v1jp
      double precision v3qp,v3jp,egrnd
      double precision xij(nmax,nmax),yij(nmax,nmax),zij(nmax,nmax)
      double precision rij(nmax,nmax)
      double precision dist,rcut,e3,e1,v3,v1,vsp,zxa,zxb
c     for LAPACK compatibility:
      complex*16 vec_gs(natz),vec_gsold(natz)
      complex*16 A(natz,natz),tau(natz)
      double precision diag(natz),eoffdiag(natz)
      integer info,iu,nbvp,isuppz(2*natz)
      double precision vpmin,vpmax,abstol
      character*1 uplo,jobz,range,order
      integer iblock,isplit
      dimension iblock(natz),isplit(natz)
      integer lda,ldz,liwork,lrwork,lwork
      parameter(lda=natz,ldz=natz,liwork=10*natz,lrwork=24*natz)
      parameter(lwork=65*natz)
      complex*16 work(lwork)
      double precision rwork(lrwork)
      integer iwork(liwork)
      external x04daf,zheevr
      complex*16 vecp(ldz,natz)
c     relativity and spin-orbit components
      double precision e11(natz),dd(natz)

      nbas=12*natoms+1

c     following for LAPACK:
c     building of complex Hamiltonian
c      do i=1,nbas
c         do j=1,nbas
c            A(j,i)=dcmplx(har(j,i),hai(j,i))
c         enddo
c      enddo
c     diagonalization
c      il=1
c      iu=1
c      vpmin=-1D10
c      vpmax=1D10
c      abstol=1.D-15
c      nbvp=12
c      info=1
c      call ZHEEVR('V','I','U',nbas,A,natz,vpmin,vpmax,il,iu,abstol,nbvp,
c     &vp,vecp,ldz,isuppz,work,lwork,rwork,lrwork,iwork,liwork,info)

c     diagonalization

      if (relativistic) then
         call F02AXF(Har,natz,Hai,natz,nbas,vp,vecr,natz,veci,natz,
     &        wa,wb,wc,ifail)
         do i=1,nbas
            vec_gs(i)=dcmplx(vecr(i,1),veci(i,1))
         enddo
      else
         do i=1,nbas
            do j=1,nbas
               har_save(i,j)=har(i,j)
               hai(i,j)=0.D0
            enddo
         enddo
         call tred2(har_save,nbas,natz,dd,e11)
         call tqli(dd,e11,nbas,natz,har_save)
         call eigsrt(dd,har_save,nbas,natz)
         do i=1,nbas
            vec_gs(i)=dcmplx(har_save(i,1),0.D0)
            vp(i)=dd(i)
         enddo
      endif

      henergy=0.D0

      return
      end
c__________________________________________________________________________

      function henergy_PT(natoms,har,hai,vec_gs)
      implicit none
      integer nmax,natoms,natz
      parameter(nmax=129,natz=12*nmax+1)
      double precision x(nmax),y(nmax),z(nmax)
c     for LAPACK compatibility:
      complex*16 vec_gs(natz),vec_gsold(natz)
      double precision henergy_PT
      double precision Har(natz,natz),Hai(natz,natz)
      double precision SOar(12,12),SOai(12,12),WA(natz)
      double precision WB(natz),WC(natz),rot(3,3)
      integer ind(natz)
      integer i,j,nbas,indf,ii,l,k,jj,idum,ib,jb,iisave,ik,il
      double precision zeta,e3p,e1p,V1qs,V1js,V0,Vt,v3qs,v3js,v1qp,v1jp
      double precision v3qp,v3jp,egrnd
      double precision xij(nmax,nmax),yij(nmax,nmax),zij(nmax,nmax)
      double precision rij(nmax,nmax)
      double precision dist,rcut,e3,e1,v3,v1,vsp,zxa,zxb
      double precision verr(natz),veii(natz),veir(natz),veri(natz)
c     relativity and spin-orbit components

      nbas=12*natoms+1

c     1st perturbation estimation from reference eigenvectors

      do j=1,nbas
         verr(j)=0.D0
         veii(j)=0.D0
         veri(j)=0.D0
         veir(j)=0.D0
         do k=1,nbas
            verr(j)=verr(j)+har(j,k)*dble(vec_gs(k))
            veir(j)=veir(j)+hai(j,k)*dble(vec_gs(k))
            if (relativistic) then
               veii(j)=veii(j)+hai(j,k)*dimag(vec_gs(k))
               veri(j)=veri(j)+har(j,k)*dimag(vec_gs(k))
            endif
         enddo
      enddo
      henergy_PT=0.D0
      do j=1,nbas
         henergy_PT=henergy_PT+dble(vec_gs(j))*verr(j)
     &        +dimag(vec_gs(j))*veri(j)
         henergy_PT=henergy_PT+dimag(vec_gs(j))*veir(j)
     &        -dble(vec_gs(j))*veii(j)
      enddo

      return
      end
