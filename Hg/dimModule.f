      MODULE DIM_MODULE
      IMPLICIT NONE
      logical relativistic,periodic,ovrlap
      contains
      include 'dimer.f'
      include 'matrot.f'
      include 'hamilton.f'
      END MODULE DIM_MODULE
