      SUBROUTINE F02AXF(AR, IAR, AI, IAI, N, WR, VR, IVR, VI, IVI,WK1,
     * WK2, WK3, IFAIL)
      implicit none
      INTEGER  ISAVE, IFAIL, I, N, J, IVR, IVI, IAR, IAI
      DOUBLE PRECISION MAX, XXXX, SUM, SQ, A, B, AR(IAR,N), AI(IAI,N),
     * WR(N),VR(IVR,N), VI(IVI,N), WK1(N), WK2(N), WK3(N)
C
C     EIGENVALUES AND EIGENVECTORS OF A COMPLEX HERMITIAN MATRIX
C     1ST APRIL 1972
C
      ISAVE = IFAIL
      DO 40 I=1,N
         IF (AI(I,I).NE.0.0D0) GO TO 140
         DO 20 J=1,I
            VR(I,J) = AR(I,J)
            VI(I,J) = AI(I,J)
   20    CONTINUE
   40 CONTINUE
      CALL F01BCF(N, X02ADF(XXXX), VR, IVR, VI, IVI, WR, WK1, WK2,WK3)
      IFAIL = 1
      CALL F02AYF(N, X02AAF(XXXX), WR, WK1, VR, IVR, VI, IVI, IFAIL)
      IF (IFAIL.EQ.0) GO TO 60
      IFAIL = 1
      RETURN
C     NORMALIZES
   60 DO 120 I=1,N
         SUM = 0.0D0
         MAX = 0.0D0
         DO 80 J=1,N
            SQ = VR(J,I)*VR(J,I) + VI(J,I)*VI(J,I)
            SUM = SUM + SQ
            IF (SQ.LE.MAX) GO TO 80
            MAX = SQ
            A = VR(J,I)
            B = VI(J,I)
   80    CONTINUE
         IF (SUM.EQ.0.0D0) GO TO 120
         SUM = 1.0D0/DSQRT(SUM*MAX)
         DO 100 J=1,N
            SQ = SUM*(VR(J,I)*A+VI(J,I)*B)
            VI(J,I) = SUM*(VI(J,I)*A-VR(J,I)*B)
            VR(J,I) = SQ
  100    CONTINUE
  120 CONTINUE
      RETURN
  140 IFAIL = 2
      RETURN
      END
c_______________________________________________________________________

      SUBROUTINE F01BCF(N, TOL, Z, IZ, W, IW, D, E, C, S)
C     MARK 3 RELEASE. NAG COPYRIGHT 1972.
C     MARK 4 REVISED.
C     MARK 4.5 REVISED
C     MARK 6B REVISED IER-125 IER-127  (JUL 1978)
      implicit none
      INTEGER N, II, I, L, K, J, J1, IZ, IW
      DOUBLE PRECISION G,FR,FI,H,R,CO,SI,TOL,GR,GI,F,HH,
     & Z(IZ,N), W(IW,N), D(N), E(N), C(N), S(N)
C
C     TRECX2
C     F01BCF REDUCES A COMPLEX*16 HERMITIAN MATRIX TO REAL
C     TRIDIAGONAL FORM FROM WHICH THE EIGENVALUES AND EIGENVECTORS
C     CAN BE FOUND USING SUBROUTINE F02AYF,(CXTQL2). THE HERMITIAN
C     MATRIX A=A(1) IS REDUCED TO THE TRIDIAGONAL MATRIX A(N-1) BY
C     N-2 UNITARY TRANSFORMATIONS. THE HOUSEHOLDER REDUCTION ITSELF
C     DOES NOT GIVE A REAL TRIDIAGONAL MATRIX, THE OFF-DIAGONAL
C     ELEMENTS ARE COMPLEX*16. THEY ARE SUBSEQUENTLY MADE REAL BY A
C     DIAGONAL TRANSFORMATION.
C     APRIL 1ST. 1972
C
      IF (N.EQ.1) GO TO 320
      DO 300 II=2,N
         I = N - II + 2
         L = I - 2
         G = 0.0D0
         FR = Z(I,I-1)
         FI = W(I,I-1)
         IF (L.EQ.0) GO TO 40
         DO 20 K=1,L
            G = G + Z(I,K)*Z(I,K) + W(I,K)*W(I,K)
   20    CONTINUE
   40    H = G + FR*FR + FI*FI
         IF (DABS(FR)+DABS(FI).NE.0.0D0) GO TO 60
         R = 0.0D0
         CO = 1.0D0
         C(I) = 1.0D0
         SI = 0.0D0
         S(I) = 0.0D0
         GO TO 120
   60    IF (DABS(FR).LT.DABS(FI)) GO TO 80
         R = DABS(FR)*DSQRT(1.0D0+(FI/FR)**2)
         GO TO 100
   80    R = DABS(FI)*DSQRT(1.0D0+(FR/FI)**2)
  100    SI = FI/R
         S(I) = SI
         CO = FR/R
         C(I) = CO
  120    IF (G.LE.TOL) GO TO 260
         L = L + 1
C     L IS NOW I-1
         G = -DSQRT(H)
         E(I) = G
C     E(I) HAS ITS FINAL REAL VALUE
         H = H - R*G
C     S*S + SR
         Z(I,I-1) = (R-G)*CO
         W(I,I-1) = (R-G)*SI
         FR = 0.0D0
         DO 200 J=1,L
            Z(J,I) = Z(I,J)/H
            W(J,I) = -W(I,J)/H
            GR = 0.0D0
            GI = 0.0D0
C     NOW FORM ELEMENT OF AU IN GR AND GI
            DO 140 K=1,J
               GR = GR + Z(J,K)*Z(I,K) + W(J,K)*W(I,K)
               GI = GI - Z(J,K)*W(I,K) + W(J,K)*Z(I,K)
  140       CONTINUE
            J1 = J + 1
            IF (J1.GT.L) GO TO 180
            DO 160 K=J1,L
               GR = GR + Z(K,J)*Z(I,K) - W(K,J)*W(I,K)
               GI = GI - Z(K,J)*W(I,K) - W(K,J)*Z(I,K)
  160       CONTINUE
C     NOW FORM ELEMENT OF P
  180       C(J) = GR/H
            S(J) = GI/H
C     C(J)+I S(J)= AU(J)/H= P(J)
            FR = FR + GR*Z(J,I) + GI*W(J,I)
C     WE MAKE THE ADDITION OF
C     AU(J)*UBAR(J)/H TO ACCUMULATING
C     U(H)AU, WE NEED ONLY THE REAL PART
  200    CONTINUE
         HH = FR/(H+H)
C     NOW FORM REDUCED A
         DO 240 J=1,L
            FR = Z(I,J)
            FI = -W(I,J)
            GR = C(J) - HH*FR
            C(J) = GR
            GI = S(J) - HH*FI
            S(J) = GI
C     F= U(J), H= K, P(J)= -K*U(J) = Q(J)
            DO 220 K=1,J
               Z(J,K) = Z(J,K) - FR*C(K) - FI*S(K) - GR*Z(I,K) +
     &          GI*W(I,K)
               W(J,K) = W(J,K) + FR*S(K) - FI*C(K) - GR*W(I,K) -
     &          GI*Z(I,K)
  220       CONTINUE
            W(J,J) = 0.0D0
  240    CONTINUE
C     A(J,K)= A(J,K)-U(J)*QBAR(K)-Q(J)*UBAR(K)
C     = Z(J,K)
C     = I W(J,K)-(FR+I FI)*(C(K)-I S(K)) -
C     (GR+I GI)*(Z(I,K)+I W(I,K))
         GO TO 280
  260    E(I) = R
         H = 0.0D0
  280    D(I) = H
  300 CONTINUE
  320 D(1) = 0.0D0
      E(1) = 0.0D0
C     WE NOW FORM THE PRODUCT OF THE
C     HOUSEHOLDER MATRICES, OVERWRITING
C     ON Z AND W
      DO 440 I=1,N
         L = I - 1
         IF (D(I).EQ.0.0D0) GO TO 400
         DO 380 J=1,L
            GR = 0.0D0
            GI = 0.0D0
            DO 340 K=1,L
               GR = GR + Z(I,K)*Z(K,J) - W(I,K)*W(K,J)
               GI = GI + W(I,K)*Z(K,J) + Z(I,K)*W(K,J)
  340       CONTINUE
C     WE HAVE JUST FORMED U(H)X(J)
            DO 360 K=1,L
               Z(K,J) = Z(K,J) - GR*Z(K,I) + GI*W(K,I)
               W(K,J) = W(K,J) - GR*W(K,I) - GI*Z(K,I)
  360       CONTINUE
C     WE HAVE JUST SUBTRACTED U(H)X(J)*U/H
  380    CONTINUE
  400    D(I) = Z(I,I)
         Z(I,I) = 1.0D0
         W(I,I) = 0.0D0
         IF (L.EQ.0) GO TO 440
         DO 420 J=1,L
            Z(I,J) = 0.0D0
            Z(J,I) = 0.0D0
            W(I,J) = 0.0D0
            W(J,I) = 0.0D0
  420    CONTINUE
  440 CONTINUE
C     NOW WE MULTIPLY BY THE
C     COSTHETA + I SINTHETA COLUMN
C     FACTORS
      CO = 1.0D0
      SI = 0.0D0
      IF (N.EQ.1) RETURN
      DO 480 I=2,N
         F = CO*C(I) - SI*S(I)
         SI = CO*S(I) + SI*C(I)
         CO = F
         DO 460 J=1,N
            F = Z(J,I)*CO - W(J,I)*SI
            W(J,I) = Z(J,I)*SI + W(J,I)*CO
            Z(J,I) = F
  460    CONTINUE
  480 CONTINUE
      RETURN
      END
c_______________________________________________________________________

      SUBROUTINE F02AYF(N, EPS, D, E, Z, IZ, W, IW, IFAIL)
C     MARK 3 RELEASE. NAG COPYRIGHT 1972.
C     MARK 4 REVISED.
C     MARK 4.5 REVISED
C     MARK 9 REVISED. IER-326 (SEP 1981).
C
C     CXTQL2
C     THIS SUBROUTINE FINDS THE EIGENVALUES AND EIGENVECTORS OF A
C     HERMITIAN MATRIX, WHICH HAS BEEN REDUCED TO A REAL
C     TRIDIAGONAL MATRIX, T, GIVEN WITH ITS DIAGONAL ELEMENTS IN
C     THE ARRAY D(N) AND ITS SUB-DIAGONAL ELEMENTS IN THE LAST N
C     - 1 STORES OF THE ARRAY E(N), USING QL TRANSFORMATIONS. THE
C     EIGENVALUES ARE OVERWRITTEN ON THE DIAGONAL ELEMENTS IN THE
C     ARRAY D IN ASCENDING ORDER. THE REAL AND IMAGINARY PARTS OF
C     THE EIGENVECTORS ARE FORMED IN THE ARRAYS Z,W(N,N)
C     RESPECTIVELY, OVERWRITING THE ACCUMULATED TRANSFORMATIONS AS
C     SUPPLIED BY THE SUBROUTINE F01BCF. THE SUBROUTINE WILL FAIL
C     IF ALL EIGENVALUES TAKE MORE THAN 30*N ITERATIONS
C     1ST APRIL 1972
C
      implicit none
      INTEGER  ISAVE, IFAIL, N, I, L, J, M, I1, M1, II, K,IZ, IW
C$P 1
c      DOUBLE PRECISION SRNAME
      DOUBLE PRECISION B, F, H, EPS, G, P, R, C, S, D(N), E(N), Z(IZ,N),
     * W(IW,N)
c      DATA SRNAME /8H F02AYF /
      ISAVE = IFAIL
      IF (N.EQ.1) GO TO 40
      DO 20 I=2,N
         E(I-1) = E(I)
   20 CONTINUE
   40 E(N) = 0.0D0
      B = 0.0D0
      F = 0.0D0
      J = 30*N
      DO 300 L=1,N
         H = EPS*(DABS(D(L))+DABS(E(L)))
         IF (B.LT.H) B = H
C     LOOK FOR SMALL SUB-DIAG ELEMENT
         DO 60 M=L,N
            IF (DABS(E(M)).LE.B) GO TO 80
   60    CONTINUE
   80    IF (M.EQ.L) GO TO 280
  100    IF (J.LE.0) GO TO 400
         J = J - 1
C     FORM SHIFT
         G = D(L)
         H = D(L+1) - G
         IF (DABS(H).GE.DABS(E(L))) GO TO 120
         P = H*0.5D0/E(L)
         R = DSQRT(P*P+1.0D0)
         H = P + R
         IF (P.LT.0.0D0) H = P - R
         D(L) = E(L)/H
         GO TO 140
  120    P = 2.0D0*E(L)/H
         R = DSQRT(P*P+1.0D0)
         D(L) = E(L)*P/(1.0D0+R)
  140    H = G - D(L)
         I1 = L + 1
         IF (I1.GT.N) GO TO 180
         DO 160 I=I1,N
            D(I) = D(I) - H
  160    CONTINUE
  180    F = F + H
C     QL TRANSFORMATION
         P = D(M)
         C = 1.0D0
         S = 0.0D0
         M1 = M - 1
         DO 260 II=L,M1
            I = M1 - II + L
            G = C*E(I)
            H = C*P
            IF (DABS(P).LT.DABS(E(I))) GO TO 200
            C = E(I)/P
            R = DSQRT(C*C+1.0D0)
            E(I+1) = S*P*R
            S = C/R
            C = 1.0D0/R
            GO TO 220
  200       C = P/E(I)
            R = DSQRT(C*C+1.0D0)
            E(I+1) = S*E(I)*R
            S = 1.0D0/R
            C = C/R
  220       P = C*D(I) - S*G
            D(I+1) = H + S*(C*G+S*D(I))
C     FORM VECTOR
            DO 240 K=1,N
               H = Z(K,I+1)
               Z(K,I+1) = S*Z(K,I) + C*H
               Z(K,I) = C*Z(K,I) - S*H
               H = W(K,I+1)
               W(K,I+1) = S*W(K,I) + C*H
               W(K,I) = C*W(K,I) - S*H
  240       CONTINUE
  260    CONTINUE
         E(L) = S*P
         D(L) = C*P
         IF (DABS(E(L)).GT.B) GO TO 100
  280    D(L) = D(L) + F
  300 CONTINUE
C     ORDER EIGEN VALUES ANS EIGENVECTORS
      DO 380 I=1,N
         K = I
         P = D(I)
         I1 = I + 1
         IF (I1.GT.N) GO TO 340
         DO 320 J=I1,N
            IF (D(J).GE.P) GO TO 320
            K = J
            P = D(J)
  320    CONTINUE
  340    IF (K.EQ.I) GO TO 380
         D(K) = D(I)
         D(I) = P
         DO 360 J=1,N
            P = Z(J,I)
            Z(J,I) = Z(J,K)
            Z(J,K) = P
            P = W(J,I)
            W(J,I) = W(J,K)
            W(J,K) = P
  360    CONTINUE
  380 CONTINUE
      IFAIL = 0
      RETURN
  400 IFAIL = 1
      RETURN
      END
c_______________________________________________________________________

      DOUBLEPRECISION FUNCTION X02AAF(X)
      DOUBLEPRECISION X,Z
C     NAG COPYRIGHT 1975
C     MARK 4.5 RELEASE
C     * MACHEPS *
C     RETURNS THE VALUE MACHEPS WHERE MACHEPS IS THE SMALLEST POSITIVE
C     NUMBER SUCH THAT 1.0 + EPS > 1.0
C     THE X PARAMETER IS NOT USED
      REAL MACHEP
      DIMENSION MACHEP(2)
C     DATA MACHEP(1)/O606400000000/,MACHEP(2)/O000000000000/
C      DATA MACHEP/ZC34000000,Z00000000/
C     EQUIVALENCE (Z,MACHEP(1))
C     X02AAF=Z
      X02AAF=7.105D-15
      RETURN
      END
c_______________________________________________________________________

      DOUBLEPRECISION FUNCTION X02ADF(X)
      DOUBLEPRECISION X,Z
C     NAG COPYRIGHT 1975
C     MARK 4.5 RELEASE
C     * MINTOEPS *
C     RETURNS THE RATIO OF THE SMALLEST POSITIVE REAL FLOATING-
C     POINT NUMBER REPRESENTABLE ON THE COMPUTER TO MACHEP
      REAL MINTEP
      DIMENSION MINTEP(2)
C     DATA MINTEP(1)/O574400000000/,MINTEP(2)/O000000000001/
C       DATA MINTEP/ZBE4000000,Z00000001/
C     EQUIVALENCE (Z,MINTEP(1))
C     X02ADF=Z
      X02ADF=4.0816D-25
      RETURN
      END
c_______________________________________________________________________

      SUBROUTINE tred2(a,n,np,d,e)
      IMPLICIT double precision(A-H,O-Z)
      INTEGER n,np
      DOUBLE PRECISION a(np,np),d(np),e(np)
      INTEGER i,j,k,l
      DOUBLE PRECISION f,g,h,hh,scale
      do 18 i=n,2,-1
        l=i-1
        h=0.d0
        scale=0.d0
        if(l.gt.1)then
          do 11 k=1,l
            scale=scale+dabs(a(i,k))
11        continue
          if(scale.eq.0.d0)then
            e(i)=a(i,l)
          else
            do 12 k=1,l
              a(i,k)=a(i,k)/scale
              h=h+a(i,k)**2
12          continue
            f=a(i,l)
            g=-dsign(dsqrt(h),f)
            e(i)=scale*g
            h=h-f*g
            a(i,l)=f-g
            f=0.d0
            do 15 j=1,l
C     Omit following line if finding only eigenvalues
              a(j,i)=a(i,j)/h
              g=0.d0
              do 13 k=1,j
                g=g+a(j,k)*a(i,k)
13            continue
              do 14 k=j+1,l
                g=g+a(k,j)*a(i,k)
14            continue
              e(j)=g/h
              f=f+e(j)*a(i,j)
15          continue
            hh=f/(h+h)
            do 17 j=1,l
              f=a(i,j)
              g=e(j)-hh*f
              e(j)=g
              do 16 k=1,j
                a(j,k)=a(j,k)-f*e(k)-g*a(i,k)
16            continue
17          continue
          endif
        else
          e(i)=a(i,l)
        endif
        d(i)=h
18    continue
C     Omit following line if finding only eigenvalues.
      d(1)=0.d0
      e(1)=0.d0
      do 24 i=1,n
C     Delete lines from here ...
        l=i-1
        if(d(i).ne.0.d0)then
          do 22 j=1,l
            g=0.d0
            do 19 k=1,l
              g=g+a(i,k)*a(k,j)
19          continue
            do 21 k=1,l
              a(k,j)=a(k,j)-g*a(k,i)
21          continue
22        continue
        endif
C     ... to here when finding only eigenvalues.
        d(i)=a(i,i)
C     Also delete lines from here ...
        a(i,i)=1.d0
        do 23 j=1,l
          a(i,j)=0.d0
          a(j,i)=0.d0
23      continue
C     ... to here when finding only eigenvalues.
24    continue
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software 'k'1k30m,t+W.

c_________________________________________________________________________

      SUBROUTINE tqli(d,e,n,np,z)
      IMPLICIT double precision(A-H,O-Z)
      INTEGER n,np
      DOUBLE PRECISION d(np),e(np),z(np,np)
CU    USES pythag
      INTEGER i,iter,k,l,m
      DOUBLE PRECISION b,c,dd,f,g,p,r,s
      do 11 i=2,n
        e(i-1)=e(i)
11    continue
      e(n)=0.d0
      do 15 l=1,n
        iter=0
1       do 12 m=l,n-1
          dd=dabs(d(m))+dabs(d(m+1))
          if (dabs(e(m))+dd.eq.dd) goto 2
12      continue
        m=n
2       if(m.ne.l)then
          if(iter.eq.50) print *,'too many iterations in tqli'
          iter=iter+1
          g=(d(l+1)-d(l))/(2.d0*e(l))
          r=pythag(g,1.d0)
          g=d(m)-d(l)+e(l)/(g+dsign(r,g))
          s=1.d0
          c=1.d0
          p=0.d0
          do 14 i=m-1,l,-1
            f=s*e(i)
            b=c*e(i)
            r=pythag(f,g)
            e(i+1)=r
            if(r.eq.0.d0)then
              d(i+1)=d(i+1)-p
              e(m)=0.d0
              goto 1
            endif
            s=f/r
            c=g/r
            g=d(i+1)-p
            r=(d(i)-g)*s+2.d0*c*b
            p=s*r
            d(i+1)=g+p
            g=c*r-b
C     Omit lines from here ...
            do 13 k=1,n
              f=z(k,i+1)
              z(k,i+1)=s*z(k,i)+c*f
              z(k,i)=c*z(k,i)-s*f
13          continue
C     ... to here when finding only eigenvalues.
14        continue
          d(l)=d(l)-p
          e(l)=g
          e(m)=0.d0
          goto 1
        endif
15    continue
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software 'k'1k30m,t+W.
c_________________________________________________________________________

      FUNCTION pythag(a,b)
      IMPLICIT double precision(A-H,O-Z)
      DOUBLE PRECISION a,b,pythag
      DOUBLE PRECISION absa,absb
      absa=dabs(a)
      absb=dabs(b)
      if(absa.gt.absb)then
        pythag=absa*dsqrt(1.d0+(absb/absa)**2)
      else
        if(absb.eq.0.d0)then
          pythag=0.d0
        else
          pythag=absb*dsqrt(1.d0+(absa/absb)**2)
        endif
      endif
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software 'k'1k30m,t+W.
c_________________________________________________________________________

      SUBROUTINE eigsrt(d,v,n,np)
      IMPLICIT double precision(A-H,O-Z)
      INTEGER n,np
      DOUBLE PRECISION d(np),v(np,np)
      INTEGER i,j,k
      DOUBLE PRECISION p
      do 13 i=1,n-1
        k=i
        p=d(i)
        do 11 j=i+1,n
          if(d(j).le.p)then
            k=j
            p=d(j)
          endif
11      continue
        if(k.ne.i)then
          d(k)=d(i)
          d(i)=p
          do 12 j=1,n
            p=v(j,i)
            v(j,i)=v(j,k)
            v(j,k)=p
12        continue
        endif
13    continue
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software 'k'1k30m,t+W.

